--EXEC sp_changedbowner "HOME";
--SELECT * FROM Products WHERE UnitPrice BETWEEN 15 AND 30
--SELECT * FROM Products WHERE ProductName LIKE 'C%'
--SELECT * FROM Products WHERE ProductName LIKE '%b%'
--SELECT * FROM Products WHERE ProductName LIKE '%eso%'
--SELECT * FROM Products WHERE ProductName LIKE '%s'

--SELECT p.ProductName, o.Quantity, o.UnitPrice 
--FROM Products p
--INNER JOIN [Order Details] o ON p.ProductID = o.ProductID

--SELECT p.ProductName, o.Quantity, o.UnitPrice, ord.OrderDate, c.ContactName
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--JOIN Customers c
--ON c.CustomerID = ord.CustomerID
--ORDER BY p.ProductName;

--SELECT p.ProductName, o.Quantity, o.UnitPrice, ord.OrderDate
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--JOIN Customers c
--ON c.CustomerID = ord.CustomerID
--Where YEAR(ord.OrderDate)=1997
--ORDER BY  o.UnitPrice DESC;

--SELECT p.ProductName, o.Quantity, o.UnitPrice, ord.OrderDate
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--JOIN Customers c
--ON c.CustomerID = ord.CustomerID
--Where YEAR(ord.OrderDate)<1997
--ORDER BY  o.UnitPrice DESC;

--SELECT TOP 7  p.ProductName, o.Quantity, o.UnitPrice, ord.OrderDate, c.ContactName
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--JOIN Customers c
--ON c.CustomerID = ord.CustomerID
--ORDER BY o.UnitPrice ASC;

--SELECT p.ProductName, SUM(o.UnitPrice) AS UnitPrice
--FROM Products p
--INNER JOIN [Order Details] o ON p.ProductID = o.ProductID
--group by p.ProductName,  o.UnitPrice  
--ORDER BY o.UnitPrice ASC;

--SELECT p.ProductName, SUM(o.Quantity) AS Quantity
--FROM Products p
--INNER JOIN [Order Details] o ON p.ProductID = o.ProductID
--group by p.ProductName,  o.Quantity
--ORDER BY o.Quantity ASC;

--SELECT p.ProductName, SUM(o.UnitPrice) AS UnitPrice
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--WHERE YEAR(ord.OrderDate)=1997
--GROUP BY p.ProductName,o.UnitPrice 
--ORDER BY  o.UnitPrice DESC;

--SELECT TOP 8 p.ProductName, SUM(o.UnitPrice) AS UnitPrice, ord.OrderDate
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--GROUP BY p.ProductName,  o.UnitPrice, ord.OrderDate
--HAVING YEAR(ord.OrderDate)=1997
--ORDER BY  o.UnitPrice DESC;

--SELECT o.UnitPrice, c.ContactName
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--JOIN Customers c
--ON c.CustomerID = ord.CustomerID
--GROUP BY o.UnitPrice, c.ContactName
--HAVING SUM(o.UnitPrice)>500
--ORDER BY  o.UnitPrice DESC;

--SELECT o.UnitPrice, c.ContactName
--FROM Products p
--JOIN [Order Details] o 
--ON p.ProductID = o.ProductID
--JOIN Orders ord
--ON o.OrderID = ord.OrderID
--JOIN Customers c
--ON c.CustomerID = ord.CustomerID
--GROUP BY c.ContactName, o.UnitPrice
--HAVING SUM(o.UnitPrice)<1000
--ORDER BY  o.UnitPrice DESC;