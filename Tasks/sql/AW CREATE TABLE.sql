--create database AW_Reception;
--use AW_Reception;

--CREATE TABLE Patient
--(Number_of_med_cards integer primary key  NOT NULL identity,
--Name varchar(20) NOT NULL,
--Surname varchar(20) NOT NULL,
--Middle_name varchar(20) NOT NULL,
--Sex varchar(10) NOT NULL,
--Handling varchar(10) NOT NULL,
--Birthday date NOT NULL,
--Position varchar(30) NOT NULL,
--Period_of_nutrition varchar(10),
--Number_of_contracts  varchar(20)  NOT NULL,
--Number_of_category integer NOT NULL,
--);

--CREATE TABLE Staff_in_Compartment
--(Number_of_compartment integer primary key  NOT NULL,
--Number_of_staff integer  NOT NULL
--);

--CREATE TABLE Patient_in_Compartments
--(Number_of_med_cards integer primary key  NOT NULL ,
--Number_of_compartment integer  NOT NULL
--);

--CREATE TABLE Place_of_residence
--(Number_of_med_cards integer primary key  NOT NULL ,
--Residence varchar(10) NOT NULL,
--Region integer NOT NULL,
--District varchar(40) NOT NULL,
--Town integer NOT NULL,
--Addresses varchar(40) NOT NULL,
--Phone_number integer NOT NULL,
--Place_of_residence varchar(20) NOT NULL
--);

--CREATE TABLE Region
--(Number_of_region integer primary key  NOT NULL identity,
--Name_region varchar(40) NOT NULL,
--Town_of_region integer
--);

--CREATE TABLE Town
--(Town_of_region integer primary key  NOT NULL identity,
--Name_Town varchar(40) NOT NULL
--);

--CREATE TABLE Review
--(Number_of_review integer primary key  NOT NULL identity,
--Pediculosis varchar(3) NOT NULL,
--Date_Pediculosis date,
--Scabies varchar(3) NOT NULL,
--Date_Scabies date,
--HIV varchar(3) NOT NULL,
--Date_HIV date,
--Hepatitis varchar(3) NOT NULL,
--Date_Hepatitis date,
--RW varchar(3) NOT NULL,
--Date_RW date,
--Pulse float NOT NULL,
--Systolic_pressure float NOT NULL,
--Ciastolic_pressure float NOT NULL,
--Sensitivity varchar(40),
--Blood_type integer NOT NULL,
--Rh_factor varchar(1) NOT NULL,
--Date_blood date,
--Number_of_med_cards integer NOT NULL
--);

--CREATE TABLE �ategory_patients
--(Number_of_category integer primary key  NOT NULL identity,
--�ategory_type varchar(40) NOT NULL
--);

--CREATE TABLE Compartment
--(Number_of_compartment integer primary key  NOT NULL identity,
--Name_of_compartment varchar(40) NOT NULL,
--);

--CREATE TABLE Staff
--(Number_of_staff integer primary key  NOT NULL identity,
--Name varchar(20) NOT NULL,
--Surname varchar(20) NOT NULL,
--Middle_name varchar(20) NOT NULL,
--Birthday date NOT NULL,
--Employment_day date NOT NULL,
--Addresses varchar(40) NOT NULL,
--Phone_number integer NOT NULL,
--Number_of_position integer  NOT NULL
--);

--CREATE TABLE Position
--(Number_of_position integer primary key  NOT NULL identity,
--Name_of_position varchar(40) NOT NULL
--);

--CREATE TABLE Work_schedule
--(Number_of_work_schedule integer primary key  NOT NULL identity,
--Date_of_work date NOT NULL,
--Time_of_work_beginning time NOT NULL,
--Time_of_work_ending time NOT NULL,
--Number_of_staff integer NOT NULL
--);

--CREATE TABLE Contracts
--(Number_of_contracts  varchar(20) primary key  NOT NULL,
--Company_name varchar(40) NOT NULL,
--Scope_of_the_contract varchar(40) NOT NULL
--);

--CREATE TABLE Chamber
--(Number_of_chamber integer primary key  NOT NULL,
--Number_of_compartment integer NOT NULL
--);

--CREATE TABLE Hospitalization
--(Number_of_hospitalization integer primary key  NOT NULL identity,
--Directional varchar(40) NOT NULL,
--Diagnosis_of_directional varchar(40) NOT NULL,
--Through_hours integer NOT NULL,
--Number_of_reasons integer NOT NULL,
--Diagnosis_of_clinical varchar(40) NOT NULL,
--Diagnosis_of_conclusive varchar(40) NOT NULL,
--Diagnosis_of_hospitalization varchar(40) NOT NULL,
--Date_of_hospitalization date NOT NULL,
--Number_of_med_cards integer NOT NULL
--);

--CREATE TABLE Hospitalization_reasons
--(Number_of_reasons integer primary key  NOT NULL identity,
--Reasons varchar(40) NOT NULL,
--);

--CREATE TABLE Userss
--(
--Number_of_staff integer primary key NOT NULL,
--Logins varchar(20) NOT NULL,
--Passwords integer NOT NULL,
--Roles varchar(10) NOT NULL
--);

