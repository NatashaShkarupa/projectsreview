--CREATE DATABASE Week_1;
--use Week_1
--CREATE TABLE S (
--n_post CHAR(5) NOT NULL,
--name CHAR(20) NOT NULL,
--reiting SMALLINT NOT NULL,
--town CHAR(15) NOT NULL
--);
--CREATE TABLE P (
--n_det CHAR(5) NOT NULL,
--name CHAR(20) NOT NULL,
--cvet CHAR(7) NOT NULL,
--ves SMALLINT NOT NULL,
--town CHAR(15) NOT NULL
--);
--CREATE TABLE SP (
--n_post CHAR(5) NOT NULL,
--n_det CHAR(6) NOT NULL,
--date_post DATE NOT NULL,
--kol SMALLINT NOT NULL
--);
--INSERT INTO S VALUES 
--('S1', 'Smith', '20', 'London'),
--('S2', 'Jones', '10', 'Parig'),
--('S3', 'Blake', '30', 'Parig'),
--('S4', 'Clark', '20', 'London'),
--('S5', 'Adams', '30', 'Atens');
--INSERT INTO P VALUES
--('P1','Gaika', 'Red', '12', 'London'),
--('P2','Bolt', 'Green', '17', 'Parig'),
--('P3','Vint', 'Blue', '17', 'Rim'),
--('P4','Vint', 'Red', '14', 'London'),
--('P5','Kulachock', 'Blue', '12', 'Parig'),
--('P6','Bloom', 'Red', '19', 'London');
--INSERT INTO SP (n_post, n_det, date_post, kol) VALUES 
--('S1', 'P1', '1995/01/02', '300'),
--('S1', 'P2', '1995/04/05', '200'),
--('S1', 'P3', '05/12/1995', '400'),
--('S1', 'P4', '1995/06/15', '200'),
--('S1', 'P5', '1995/07/22', '100'),
--('S1', 'P6', '1995/08/13', '100'),
--('S2', 'P1', '1995/03/03', '300'),
--('S2', 'P2', '1995/06/12', '400'),
--('S3', 'P2', '1995/04/04', '200'),
--('S4', 'P3', '1995/03/23', '200'),
--('S4', 'P4', '1995/06/17', '300'),
--('S4', 'P5', '1995/08/22', '400');
----------------1-------------------
--SELECT * FROM S;
--SELECT name, town, reiting, n_post FROM  S;

----------------2------------------
--SELECT n_det FROM P;

----------------3-----------------
--SELECT DISTINCT n_det FROM P;

----------------4
--SELECT LEFT(name, 2) as initials, reiting FROM S;

----------------5
--SELECT * FROM S ORDER BY town,reiting;

----------------6
--SELECT * FROM P WHERE name LIKE 'B%';

----------------7
--SELECT AVG(kol) as average, MIN(kol) as minimum, MAX(kol) as maximum 
--FROM SP WHERE n_post = 'S1';

----------------8
--SELECT n_det as '����� ������',
--		 kol as '����������', 
--		 DATEPART(DAY,date_post) as Day, 
--		 DATEPART(MONTH,date_post) as month,  
--		 DATEPART(DW,date_post) as weekday, 
--		 DATEDIFF(D, date_post, SYSDATETIME()) as Expression from SP;

----------------9
--SELECT * 
--	FROM S,P
--	WHERE S.town = P.town;

----------------10
--SELECT n_det AS '����� ����������', 
--	SUM(kol) AS '����� ��������' 
--	FROM SP 
--	where n_post <> 'S1' 
--		GROUP BY n_det;

--------------11
--SELECT DISTINCT P.n_det FROM SP, P
--	WHERE SP.n_det = P.n_det AND P.ves > 16 OR SP.n_post = 'S2';

--------------12
--DELETE FROM SP
--WHERE n_post = 'S1';
--DELETE FROM SP
--WHERE n_post = 'S4';
--SELECT * FROM SP;