﻿using System;
using System.Collections.Generic;

namespace Data_Access_Layer.Contractss
{
    public interface IPatientRepository
    {
        IEnumerable<Patient> GetList();
        IEnumerable<Patient> GetList(Func<Patient, bool> expression);
        void AddBook(Patient newPatient);
        void DeleteRow(int id);
        void Save();
    }
}
