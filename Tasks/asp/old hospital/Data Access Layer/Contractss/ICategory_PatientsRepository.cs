﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
    public interface ICategory_PatientsRepository
    {
        IEnumerable<Сategory_patients> GetList();
        void Save();
        void DeleteRow(int id);
    //    IEnumerable<Сategory_patients> GetList(Func<Сategory_patients, bool> expression);
        void AddCategory(Сategory_patients newCategory);
    }
}
