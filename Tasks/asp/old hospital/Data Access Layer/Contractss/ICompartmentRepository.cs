﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
    public interface ICompartmentRepository
    {
        IEnumerable<Compartment> GetList();
        void Save();
        void DeleteRow(int id);
        IEnumerable<Compartment> GetList(Func<Compartment, bool> expression);
        void AddCompartment(Compartment newComparement);
    }
}
