﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
   public interface IHospitalizationRepository
    {
        IEnumerable<Hospitalization> GetList();
        void AddHospitalization(Hospitalization newHospitalization);
    }
}
