﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
    public interface IWork_ShedulerRepository
    {
        IEnumerable<Work_schedule> GetList();
    }
}
