﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
    public interface IStaffRepository
    {
        IEnumerable<Staff> GetList();
        void AddStaff(Staff newStaff);
        IEnumerable<Staff> GetList(Func<Staff, bool> expression);
        void Save();
        void DeleteRow(int id);
    }
}
