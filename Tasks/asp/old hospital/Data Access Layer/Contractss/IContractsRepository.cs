﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
    public interface IContractsRepository
    {
        IEnumerable<Contracts> GetList();
        IEnumerable<Contracts> GetList(Func<Contracts, bool> expression);
        void AddContracts(Contracts newContracts);
        void DeleteRow(string id);
        void Save();
    }
}
