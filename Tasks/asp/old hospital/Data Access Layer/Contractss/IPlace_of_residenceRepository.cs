﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Contractss
{
    public interface IPlace_of_residenceRepository
    {
        IEnumerable<Place_of_residence> GetList();
    }
}
