//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data_Access_Layer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Place_of_residence
    {
        public int Number_of_med_cards { get; set; }
        public string Residence { get; set; }
        public string Town { get; set; }
        public Nullable<int> Region { get; set; }
        public string District { get; set; }
        public string Addresses { get; set; }
        public long Phone_number { get; set; }
        public string Place_of_residence1 { get; set; }
    
        public virtual Patient Patient { get; set; }
    }
}
