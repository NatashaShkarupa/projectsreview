﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class ModelHosp
    {
        public int Number_of_hospitalization { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Middle_name { get; set; }
        public string Handling { get; set; }
        public string Directional { get; set; }
        public string Diagnosis_of_directional { get; set; }
        public int Through_hours { get; set; }
        public string Diagnosis_of_clinical { get; set; }
        public string Diagnosis_of_conclusive { get; set; }
        public string Diagnosis_of_hospitalization { get; set; }
        public DateTime Date_of_hospitalization { get; set; }
        public string Reasons { get; set; }
        public int? Number_of_med_cards { get; set; }
    }
}
