﻿using Data_Access_Layer.Contractss;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class CompartmentRepository : BaseRepository, ICompartmentRepository
    {
        public IEnumerable<Compartment> GetList()
        {
            using (var context = GetContext())
            {
                return context.Compartment.ToList();
            }
        }
        public void AddCompartment(Compartment newComparement)
        {
            using (var context = GetContext())
            {
                context.Compartment.Add(newComparement);
                context.SaveChanges();
            }
        }

        public IEnumerable<Compartment> GetList(Func<Compartment, bool> expression)
        {
            using (var context = GetContext())
            {
                return context.Compartment.Where(expression).ToList();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var sel = context.Compartment.FirstOrDefault(x => x.Number_of_compartment == id);
                if (sel != null)
                {
                    context.Compartment.Remove(sel);
                    context.SaveChanges();
                }
            }
        }
        public void EditRow(Compartment newPatient)
        {
            using (var context = GetContext())
            {
                context.Entry(newPatient).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
