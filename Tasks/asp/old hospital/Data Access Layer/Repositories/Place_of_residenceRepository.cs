﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Access_Layer.Contractss;

namespace Data_Access_Layer.Repositories
{
    public class Place_of_residenceRepository: BaseRepository, IPlace_of_residenceRepository
    {
        public IEnumerable<Place_of_residence> GetList()
        {
            using (var context = GetContext())
            {
                return context.Place_of_residence.ToList();
            }
        }
    }
}
