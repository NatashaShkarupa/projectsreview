﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public abstract class BaseRepository
    {
        protected AW_ReceptionEntities GetContext()
        {
            return new AW_ReceptionEntities();
        }
    }
}
