﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class WorkSchedulerRepository: BaseRepository
    {
        public IEnumerable<Work_schedule> GetList()
        {
            using (var context = GetContext())
            {
                return context.Work_schedule.ToArray();
            }
        }

        public List<Model> GetListWithStaff()
        {
            using (var context = GetContext())
            {
                var query =
               from s in context.Staff
               join w in context.Work_schedule
               on s.Number_of_staff equals w.Number_of_staff
               select new Model
               {
                   Number_of_work_schedule=w.Number_of_work_schedule,
                   Name = s.Name,
                   Surname = s.Surname,
                   Middle_name = s.Middle_name,
                   Data_of_work = w.Date_of_work,
                   Time_of_beginning = w.Time_of_work_beginning,
                   Time_of_ending = w.Time_of_work_ending
               };
                return query.ToList();
            }
        }

        public void AddWork_schedule(Work_schedule newPatient)
        {
            using (var context = GetContext())
            {
                context.Work_schedule.Add(newPatient);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var patient = context.Work_schedule.FirstOrDefault(x => x.Number_of_work_schedule == id);
                if (patient != null)
                {
                    context.Work_schedule.Remove(patient);
                    context.SaveChanges();
                }
            }
        }

        public void EditRow(Work_schedule newPatient)
        {
            using (var context = GetContext())
            {
                context.Entry(newPatient).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
