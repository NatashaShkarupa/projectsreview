﻿using Data_Access_Layer.Contractss;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class ContractsRepository : BaseRepository, IContractsRepository
    {

        public IEnumerable<Contracts> GetList()
        {
            using (var context = GetContext())
            {
                return context.Contracts.ToList();
            }
        }
        public void AddContracts(Contracts newContracts)
        {
            using (var context = GetContext())
            {
                var patient = context.Contracts.FirstOrDefault(x => x.Number_of_contracts == newContracts.Number_of_contracts);
                if (patient == null)
                {
                    context.Contracts.Add(newContracts);
                    context.SaveChanges();
                }      
            }
        }

        public IEnumerable<Contracts> GetList(Func<Contracts, bool> expression)
        {
            using (var context = GetContext())
            {
                return context.Contracts.Where(expression).ToList();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(string id)
        {
            using (var context = GetContext())
            {
                var sel = context.Contracts.FirstOrDefault(x => x.Number_of_contracts == id);
                if (sel != null)
                {
                    context.Contracts.Remove(sel);
                    context.SaveChanges();
                }
            }
        }
        public void EditRow(Contracts newContracts)
        {
            using (var context = GetContext())
            {
                context.Entry(newContracts).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
