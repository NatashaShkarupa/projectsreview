﻿using Data_Access_Layer.Contractss;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class UsersRepository : BaseRepository, IUsersRepository
    {
        public IEnumerable<Userss> GetList()
        {
            using (var context = GetContext())
            {
                return context.Userss.ToList();
            }
        }
    }
}
