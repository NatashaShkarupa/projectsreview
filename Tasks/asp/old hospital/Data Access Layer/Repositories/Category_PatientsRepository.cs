﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Access_Layer.Contractss;
using System.Data.Entity;

namespace Data_Access_Layer.Repositories
{
    public class Category_PatientsRepository: BaseRepository, ICategory_PatientsRepository
    {
        public void EditRow(Сategory_patients newPatient)
        {
            using (var context = GetContext())
            {
                context.Entry(newPatient).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<Сategory_patients> GetList()
        {
            using (var context = GetContext())
            {
                return context.Сategory_patients.ToList();
            }
        }

        public void AddCategory(Сategory_patients newCategory)
        {
            using (var context = GetContext())
            {
                context.Сategory_patients.Add(newCategory);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var sel = context.Сategory_patients.FirstOrDefault(x => x.Number_of_category == id);
                if (sel != null)
                {
                    context.Сategory_patients.Remove(sel);
                    context.SaveChanges();
                }
            }
        }
    }
}
