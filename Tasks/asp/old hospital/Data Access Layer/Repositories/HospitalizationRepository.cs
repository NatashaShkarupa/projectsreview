﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Access_Layer.Contractss;
using System.Data.Entity;
using System.Collections;

namespace Data_Access_Layer.Repositories
{
    public class HospitalizationRepository:BaseRepository, IHospitalizationRepository
    {
        public void AddHospitalization(Hospitalization newHospitalization)
        {
            using (var context = GetContext())
            {
                context.Hospitalization.Add(newHospitalization);
            }
        }

        public IEnumerable<Hospitalization> GetList()
        {
            using (var context = GetContext())
            {
                return context.Hospitalization.ToList();
            }
        }

        public List<ModelHosp> GetListWithPaz()
        {
            using (var context = GetContext())
            {
                var query =
                 from p in context.Patient
                 join h in context.Hospitalization
                 on p.Number_of_med_cards equals h.Number_of_med_cards
                 select new ModelHosp
                 {
                     Name = p.Name,
                     Surname = p.Surname,
                     Middle_name = p.Middle_name,
                     Number_of_hospitalization = h.Number_of_hospitalization,
                     Handling = h.Handling,
                     Directional = h.Directional,
                     Diagnosis_of_directional = h.Diagnosis_of_directional,
                     Through_hours = h.Through_hours,
                     Diagnosis_of_clinical = h.Diagnosis_of_clinical,
                     Diagnosis_of_conclusive = h.Diagnosis_of_conclusive,
                     Diagnosis_of_hospitalization = h.Diagnosis_of_hospitalization,
                     Date_of_hospitalization = h.Date_of_hospitalization,
                     Number_of_med_cards = h.Number_of_med_cards,
                     Reasons = h.Reasons
                 };
                return query.ToList();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var patient = context.Hospitalization.FirstOrDefault(x => x.Number_of_hospitalization == id);
                if (patient != null)
                {
                    context.Hospitalization.Remove(patient);
                    context.SaveChanges();
                }
                //    var departName = "Name";
                //  var patlist = context.Compartment.Where(x => x.Name_of_compartment == departName).Select(s => s.Patient).ToList();
            }
        }

        public void EditRow(Hospitalization newPatient)
        {
            using (var context = GetContext())
            {
                context.Entry(newPatient).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
