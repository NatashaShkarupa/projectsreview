﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Access_Layer.Contractss;

namespace Data_Access_Layer.Repositories
{
     public class ReviwRepository :BaseRepository, IReviwRepository
    {
        public IEnumerable<Review> GetList()
        {
            using (var context = GetContext())
            {
                return context.Review.ToList();
            }
        }
    }
}
