﻿using Data_Access_Layer.Contractss;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class PatientRepozitories: BaseRepository, IPatientRepository
    {
        public IEnumerable<Patient> GetList()
        {
            using (var context = GetContext())
            {
                return context.Patient.ToArray();
            }
        }

        public void AddBook(Patient newPatient)
        {
            using (var context = GetContext())
            {
                    context.Patient.Add(newPatient);
                    context.SaveChanges(); 
            }
        }

        public IEnumerable<Patient> GetList(Func<Patient, bool> expression)
        {
            using (var context = GetContext())
            {
                return context.Patient.Where(expression).ToList();
            }
        }


        public void Save()
        {
            using(var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var patient = context.Patient.FirstOrDefault(x => x.Number_of_med_cards == id);
                if (patient != null)
                {
                    context.Patient.Remove(patient);
                    context.SaveChanges();
                }
            //    var departName = "Name";
              //  var patlist = context.Compartment.Where(x => x.Name_of_compartment == departName).Select(s => s.Patient).ToList();
            }
        }

        public void EditRow(Patient newPatient)
        {
            using (var context = GetContext())
            {
                context.Entry(newPatient).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
