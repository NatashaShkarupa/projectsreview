﻿using Data_Access_Layer.Contractss;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class StaffRepository : BaseRepository, IStaffRepository
    {
        public IEnumerable<Staff> GetList()
        {
            using (var context = GetContext())
            {
                return context.Staff.ToList();
            }
        }

        public void AddStaff(Staff newStaff)
        {
            using (var context = GetContext())
            {
                context.Staff.Add(newStaff);
                context.SaveChanges();
            }
        }

        public IEnumerable<Staff> GetList(Func<Staff, bool> expression)
        {
            using (var context = GetContext())
            {
                return context.Staff.Where(expression).ToList();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var staff = context.Staff.FirstOrDefault(x => x.Number_of_staff == id);
                if (staff != null)
                {
                    context.Staff.Remove(staff);
                    context.SaveChanges();
                }
            }
        }
        public void EditRow(Staff newStaff)
        {
            using (var context = GetContext())
            {
                context.Entry(newStaff).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
