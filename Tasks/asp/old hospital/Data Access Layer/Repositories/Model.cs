﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
    public class Model
    {
        public int Number_of_work_schedule { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Middle_name { get; set; }
        public DateTime Data_of_work { get; set; }
        public TimeSpan Time_of_beginning { get; set; }
        public TimeSpan Time_of_ending { get; set; }
    }
}
