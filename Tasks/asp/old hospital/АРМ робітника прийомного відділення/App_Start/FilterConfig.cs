﻿using System.Web;
using System.Web.Mvc;

namespace АРМ_робітника_прийомного_відділення
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
