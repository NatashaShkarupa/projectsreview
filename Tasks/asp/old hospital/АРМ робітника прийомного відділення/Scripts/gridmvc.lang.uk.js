﻿window.GridMvc = window.GridMvc || {};
window.GridMvc.lang = window.GridMvc.lang || {};
GridMvc.lang.uk = {
    filterTypeLabel: "Тип фільтру: ",
    filterValueLabel: "Значення:",
    applyFilterButtonText: "Застосувати",
    filterSelectTypes: {
        Equals: "Дорівнює",
        StartsWith: "Починається з",
        Contains: "Містить",
        EndsWith: "Закінчується з",
        GreaterThan: "Більше",
        LessThan: "Меньше"
    },
    code: 'uk',
    boolTrueLabel: "Так",
    boolFalseLabel: "Ні",
    clearFilterLabel: "Очистити фільтр"
};