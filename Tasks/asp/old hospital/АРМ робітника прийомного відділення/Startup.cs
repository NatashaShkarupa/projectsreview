﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(АРМ_робітника_прийомного_відділення.Startup))]
namespace АРМ_робітника_прийомного_відділення
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
