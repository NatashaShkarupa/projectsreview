﻿using System.Linq;
using System.Web.Mvc;
using Business_Layer.Manager;
using Business_Layer.DataModels;

namespace АРМ_робітника_прийомного_відділення.Controllers
{
    public class WorkSchedulerController : Controller
    {

        private Work_scheduleManager schedule = new Work_scheduleManager();
        private Work_scheduleDataModel schedulemodel = new Work_scheduleDataModel();
        private StaffManager staff = new StaffManager();
        private StaffDataModel staffmodel = new StaffDataModel();

        public ActionResult Work_scheduleGrid()
        {
            return View(schedule.GetListOfWork_ShedulerWithStaff());
        }

        [HttpGet]
        public ActionResult CreateWork_schedule()
        {
            var model = staff.GetListOfStaff().
                Select(x => new SelectListItem
                {
                    Text = x.Surname,
                    Value = x.Number_of_staff.ToString()
                }).ToList();
            ViewBag.Work_schedule = model;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWork_schedule([Bind(Include = "Number_of_work_schedule,Data_of_work,Time_of_beginning,Time_of_ending,Number_of_staff")] Work_scheduleDataModel schedulemodel)
        {
            if (ModelState.IsValid)
            {
                schedule.AddWork_Sheduler(schedulemodel);
                return RedirectToAction("Work_scheduleGrid");
            }
            return View(schedulemodel);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteWork_schedule(int id)
        {
            try
            {
                schedule.DelRowById(id);
                return RedirectToAction("Work_scheduleGrid");
            }
            catch
            {
                return View(schedulemodel);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditWork_schedule(int id)
        {
            var model = staff.GetListOfStaff().
                Select(x => new SelectListItem
                {
                    Text = x.Surname,
                    Value = x.Number_of_staff.ToString()
                }).ToList();
            ViewBag.Work_schedule = model;
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditWork_schedule(int id, [Bind(Include = "Number_of_work_schedule,Data_of_work,Time_of_beginning,Time_of_ending,Number_of_staff")] Work_scheduleDataModel schedulemodel)
        {
            if (ModelState.IsValid)
            {
                schedule.EditPatient(id, schedulemodel);
                return RedirectToAction("Work_scheduleGrid");
            }
            return View(schedulemodel);
        }
    }
}
