﻿using System.Web.Mvc;
using Business_Layer.Manager;
using System.Net;
using Business_Layer.DataModels;
using System;

namespace АРМ_робітника_прийомного_відділення.Controllers
{
    public class HospitalizationController : Controller
    {
        private HospitalizationManager hospitalization = new HospitalizationManager();
        private HospitalizationDataModel hospitalizationmodel = new HospitalizationDataModel();

        public ActionResult HospitalizationGrid()
        {
            return View(hospitalization.GetListOfHospitalization());
        }


  
        public ActionResult HospitalizationGridWith()
        {
            return View(hospitalization.GetListOfHospitalizationWithPaz());
        }


        [HttpGet]
        public ActionResult CreateHospitalization(int? id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateHospitalization(int? id,[Bind(Include = "Number_of_hospitalization,Handling,Directional,Diagnosis_of_directional,Through_hours,Diagnosis_of_clinical,Diagnosis_of_conclusive,Diagnosis_of_hospitalization,Date_of_hospitalization,Number_of_med_cards,Reasons")] HospitalizationDataModel hospitalizationmodel)
        {
            if (ModelState.IsValid)
            {
                hospitalization.AddHospitalization(id,hospitalizationmodel);
                return RedirectToAction("HospitalizationGrid");
            }
            return View(hospitalizationmodel);
        }
        [Authorize(Roles = "admin")]
        public ActionResult DeleteHospitalization(int id)
        {
            try
            {
                hospitalization.DelRowById(id);
                return RedirectToAction("HospitalizationGrid");
            }
            catch
            {
                return View(hospitalizationmodel);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditHospitalization(int id)
        {
            return View();
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditHospitalization(int id, [Bind(Include = "Number_of_hospitalization,Handling,Directional,Diagnosis_of_directional,Through_hours,Diagnosis_of_clinical,Diagnosis_of_conclusive,Diagnosis_of_hospitalization,Date_of_hospitalization,Number_of_med_cards,Reasons")] HospitalizationDataModel hospitalizationmodel)
        {
            if (ModelState.IsValid)
            {
                hospitalization.EditPatientcategory(id, hospitalizationmodel);
                return RedirectToAction("HospitalizationGrid");
            }
            return View(hospitalizationmodel);
        }
    }
}
