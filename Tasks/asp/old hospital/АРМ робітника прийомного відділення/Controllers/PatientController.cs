﻿using System.Linq;
using System.Web.Mvc;
using Business_Layer.Manager;
using Business_Layer.DataModels;
using АРМ_робітника_прийомного_відділення.Filters;

namespace АРМ_робітника_прийомного_відділення.Controllers
{
    public class PatientController : Controller
    {
        private PatientManager patient = new PatientManager();
        private PatientDataModel patientmodel = new PatientDataModel();
        private CategoryManager category = new CategoryManager();
        private ContractsManager contract = new ContractsManager();

        public ActionResult PatientGrid()
        {
            return View(patient.GetListOfPatient());
        }

        [HttpGet]
        public ActionResult CreatePatient()
        {
            var model = category.GetListOfCategory().
                Select(x => new SelectListItem
                {
                    Text = x.Category_type,
                    Value = x.Number_of_category.ToString()
                }).ToList();
            ViewBag.CategoryTypesList = model;

            var model2 = contract.GetListOfContracts().
                Select(x => new SelectListItem
                {
                    Text = x.Number_of_contracts,
                    Value = x.Number_of_contracts
                }).ToList();
            ViewBag.ContractsTypesList = model2;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePatient([Bind(Include = "Number_of_med_cards,Name,Surname,Middle_name,Sex,Birthday,Position,Period_of_nutrition,Number_of_contracts,Number_of_category")] PatientDataModel patientmodel)
        {
            if (ModelState.IsValid)
            {
                patient.AddPatient(patientmodel);
                return RedirectToAction("PatientGrid");
            }
            return View(patientmodel);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeletePatient(int id)
        {
            try
            {
                patient.DelRowById(id);
                return RedirectToAction("PatientGrid");
            }
            catch
            {
                return View(patientmodel);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult EditPatient(int id)
        {
            var model = category.GetListOfCategory().
               Select(x => new SelectListItem
               {
                   Text = x.Category_type,
                   Value = x.Number_of_category.ToString()
               }).ToList();
            ViewBag.CategoryTypesList = model;

            var model2 = contract.GetListOfContracts().
                Select(x => new SelectListItem
                {
                    Text = x.Number_of_contracts,
                    Value = x.Number_of_contracts
                }).ToList();
            ViewBag.ContractsTypesList = model2;


            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditPatient(int id,[Bind(Include = "Number_of_med_cards,Name,Surname,Middle_name,Sex,Birthday,Position,Period_of_nutrition,Number_of_contracts,Number_of_category")] PatientDataModel patientmodel)
        {
            if (ModelState.IsValid)
            {
                patient.EditPatient(id,patientmodel);
                return RedirectToAction("PatientGrid");
            }
            return View(patientmodel);
        }
    }
}
