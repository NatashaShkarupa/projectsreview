﻿using Business_Layer.DataModels;
using Business_Layer.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace АРМ_робітника_прийомного_відділення.Controllers
{
    public class StaffController : Controller
    {
        private StaffManager staff = new StaffManager();
        private StaffDataModel staffmodel = new StaffDataModel();

        public ActionResult StaffGrid()
        {
            return View(staff.GetListOfStaff());
        }

        [HttpGet]
        public ActionResult CreateStaff()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateStaff([Bind(Include = "Number_of_staff,Name,Surname,Middle_name,Birthday,Employment_day,Addresses,Phone_number,Name_of_position")] StaffDataModel patientmodel)
        {
            if (ModelState.IsValid)
            {
                staff.AddStaff(patientmodel);
                return RedirectToAction("StaffGrid");
            }
            return View(patientmodel);
        }
        [Authorize(Roles = "admin")]
        public ActionResult DeleteStaff(int id)
        {
            try
            {
                staff.DelRowById(id);
                return RedirectToAction("StaffGrid");
            }
            catch
            {
                return View(staffmodel);
            }
        }
        [Authorize(Roles = "admin")]
        public ActionResult EditStaff(int id)
        {
            return View();
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditStaff(int id, [Bind(Include = "Number_of_staff,Name,Surname,Middle_name,Birthday,Employment_day,Addresses,Phone_number,Name_of_position")] StaffDataModel staffmodel)
        {
            if (ModelState.IsValid)
            {
                staff.EditStaff(id, staffmodel);
                return RedirectToAction("StaffGrid");
            }
            return View(staffmodel);
        }
    }
}
