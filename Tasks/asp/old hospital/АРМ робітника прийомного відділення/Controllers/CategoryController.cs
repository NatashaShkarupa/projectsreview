﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business_Layer;
using Business_Layer.Manager;
using System.Net;
using Business_Layer.DataModels;

namespace АРМ_робітника_прийомного_відділення.Controllers
{
    public class CategoryController : Controller
    {
        private CategoryManager category = new CategoryManager();
        private Category_PatientsDataModel categroymodel = new Category_PatientsDataModel();

        [HttpGet]
        public ActionResult CategoryGridViewModel()
        {
            var viewmodel = new ViewModel();
            viewmodel.Category = category.GetListOfCategory();
            viewmodel.Contract = contract.GetListOfContracts();
            viewmodel.Compartment = comparement.GetListOfCompartment();
            return View(viewmodel);
        }

        [HttpGet]
        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory([Bind(Include = "Number_of_category,Category_type")] Category_PatientsDataModel categroymodel)
        {
            if (ModelState.IsValid)
            {
                category.AddCategory(categroymodel);
                return RedirectToAction("CategoryGridViewModel");
            }
            return View(categroymodel);
        }
        [Authorize(Roles = "admin")]
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                category.DelRowById(id);
                return RedirectToAction("CategoryGridViewModel");
            }
            catch
            {
                return View(categroymodel);
            }
        }
        [Authorize(Roles = "admin")]
        public ActionResult EditCategory(int id)
        {
            return View();
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditCategory(int id, [Bind(Include = "Number_of_category,,Category_type")] Category_PatientsDataModel categorymodel)
        {
            if (ModelState.IsValid)
            {
                category.EditPatientcategory(id, categorymodel);
                return RedirectToAction("CategoryGridViewModel");
            }
            return View(categorymodel);
        }

        //control contract
        private ContractsManager contract = new ContractsManager();
        private ContractsDataModel contractmodel = new ContractsDataModel();


        public ActionResult ContractsGrid()
        {
            ViewBag.Contract = contractmodel;
            return PartialView(contract.GetListOfContracts());      
        }

        [HttpGet]
        public ActionResult CreateContracts()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateContracts([Bind(Include = "Number_of_contracts,Company_name,Scope_of_the_contracts")] ContractsDataModel contractmodel)
        {
            if (ModelState.IsValid)
            {
                contract.AddContracts(contractmodel);
                return RedirectToAction("CategoryGridViewModel");
            }
            return View(contractmodel);
        }
        [Authorize(Roles = "admin")]
        public ActionResult DeleteContracts(string id)
        {
            try
            {
                contract.DelRowById(id);
                return RedirectToAction("CategoryGridViewModel");
            }
            catch
            {
                return View(contractmodel);
            }
        }
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult EditContracts(string id)
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditContracts(string id,[Bind(Include = "Number_of_contracts,Company_name,Scope_of_the_contracts")] ContractsDataModel contractmodel)
        {
            if (ModelState.IsValid)
            {
                contract.EditContracts(id,contractmodel);
                return RedirectToAction("CategoryGridViewModel");
            }
            return View(contractmodel);
        }

        //comparement 
        private CompartmentManager comparement = new CompartmentManager();
        private CompartmentDataModel compartmentmodel = new CompartmentDataModel();

        [HttpGet]
        public ActionResult CreateCompartment()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCompartment([Bind(Include = "Number_of_compartment,Name_of_compartment")] CompartmentDataModel compartmentmodel)
        {
            if (ModelState.IsValid)
            {
                comparement.AddCompartment(compartmentmodel);
                return RedirectToAction("CategoryGridViewModel");
            }
            return View(compartmentmodel);
        }

        [Authorize(Roles = "admin")]
        public ActionResult DeleteCompartment(int id)
        {
            try
            {
                comparement.DelRowById(id);
                return RedirectToAction("CategoryGridViewModel");
            }
            catch
            {
                return View(compartmentmodel);
            }
        }
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult EditCompartment(int id)
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult EditCompartment(int id, [Bind(Include = "Number_of_compartment,Name_of_compartment")] CompartmentDataModel compartmentmodel)
        {
            if (ModelState.IsValid)
            {
                comparement.EditCompartment(id, compartmentmodel);
                return RedirectToAction("CategoryGridViewModel");
            }
            return View(contractmodel);
        }
    }
}