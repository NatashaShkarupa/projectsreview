﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Access_Layer;
using Data_Access_Layer.Contractss;
using Data_Access_Layer.Repositories;
using Business_Layer.DataModels;

namespace Business_Layer.Manager
{
    public class ContractsManager
    {
        private ContractsRepository _contractsepository = new ContractsRepository();

        public IEnumerable<ContractsDataModel> GetListOfContracts()
        {
            return _contractsepository.GetList().Select(sel => new ContractsDataModel
            {
                Number_of_contracts = sel.Number_of_contracts,
                Company_name = sel.Company_name,
                Scope_of_the_contracts=sel.Scope_of_the_contract
            });
        }

        public void AddContracts(ContractsDataModel model)
        {
            Contracts newpatient = new Contracts();
            newpatient.Number_of_contracts = model.Number_of_contracts;
            newpatient.Company_name = model.Company_name;
            newpatient.Scope_of_the_contract = model.Scope_of_the_contracts;
            _contractsepository.AddContracts(newpatient);
            _contractsepository.Save();
        }

        public IEnumerable<ContractsDataModel> GetListOfBooksByWord(string word)
        {
            return _contractsepository.GetList(x => x.Company_name.Contains(word)).Select(sel => new ContractsDataModel
            {
                Number_of_contracts = sel.Number_of_contracts,
                Company_name = sel.Company_name,
                Scope_of_the_contracts = sel.Scope_of_the_contract
            });
        }

        public void DelRowById(string id)
        {
            _contractsepository.DeleteRow(id);
        }
        public void EditContracts(string id, ContractsDataModel patientmodel)
        {
            Contracts patient = new Contracts();
            patient.Number_of_contracts = id;
            patient.Company_name = patientmodel.Company_name;
            patient.Scope_of_the_contract = patientmodel.Scope_of_the_contracts;
            _contractsepository.EditRow(patient);
        }
    }
}
