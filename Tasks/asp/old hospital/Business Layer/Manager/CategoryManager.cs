﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Access_Layer;
using Data_Access_Layer.Contractss;
using Data_Access_Layer.Repositories;
using Business_Layer.DataModels;

namespace Business_Layer.Manager
{
    public class CategoryManager
    {
        private Category_PatientsRepository _categoryRepository = new Category_PatientsRepository();

        public IEnumerable<Category_PatientsDataModel> GetListOfCategory()
        {
            return _categoryRepository.GetList().Select(sel => new Category_PatientsDataModel
            {
                Number_of_category = sel.Number_of_category,
                Category_type = sel.Сategory_type
            });
        }

        public void AddCategory(Category_PatientsDataModel sel)
        {
            Сategory_patients newpatient = new Сategory_patients();
            newpatient.Number_of_category = sel.Number_of_category;
            newpatient.Сategory_type = sel.Category_type;
            _categoryRepository.AddCategory(newpatient);
            _categoryRepository.Save();
        }

        public void DelRowById(int id)
        {
            _categoryRepository.DeleteRow(id);
        }
        public void EditPatientcategory(int id, Category_PatientsDataModel patientmodel)
        {
            Сategory_patients patient = new Сategory_patients();
            patient.Number_of_category = id;
            patient.Сategory_type = patientmodel.Category_type;
            _categoryRepository.EditRow(patient);
        }
    }
}
