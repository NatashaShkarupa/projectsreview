﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Access_Layer;
using Data_Access_Layer.Contractss;
using Data_Access_Layer.Repositories;
using Business_Layer.DataModels;


namespace Business_Layer.Manager
{
    public class StaffManager
    {
        private StaffRepository _staffRepository = new StaffRepository();

        public IEnumerable<StaffDataModel> GetListOfStaff()
        {
            return _staffRepository.GetList().Select(sel => new StaffDataModel
            {
                Number_of_staff = sel.Number_of_staff,
                Name = sel.Name,
                Surname = sel.Surname,
                Middle_name = sel.Middle_name,
                Birthday = sel.Birthday,
                Employment_day = sel.Employment_day,
                Addresses = sel.Addresses,
                Phone_number = sel.Phone_number,
                Name_of_position = sel.Name_of_position
            });
        }

        public void AddStaff(StaffDataModel newstaffmodel)
        {
            Staff newstaff = new Staff();
            newstaff.Number_of_staff = newstaffmodel.Number_of_staff;
            newstaff.Name = newstaffmodel.Name;
            newstaff.Surname = newstaffmodel.Surname;
            newstaff.Middle_name = newstaffmodel.Middle_name;
            newstaff.Birthday = newstaffmodel.Birthday;
            newstaff.Employment_day = newstaffmodel.Employment_day;
            newstaff.Addresses = newstaffmodel.Addresses;
            newstaff.Phone_number = newstaffmodel.Phone_number;
            newstaff.Name_of_position = newstaffmodel.Name_of_position;
            _staffRepository.AddStaff(newstaff);
            _staffRepository.Save();
        }

        //public IEnumerable<PatientDataModel> GetListOfBooksByWord(string word)
        //{
        //    return _patientRepository.GetList(x => x.Name.Contains(word)).Select(sel => new PatientDataModel
        //    {
        //        Number_of_med_cards = sel.Number_of_med_cards,
        //        Name = sel.Name,
        //        Surname = sel.Surname,
        //        Middle_name = sel.Middle_name,
        //        Birthday = sel.Birthday,
        //        Sex = sel.Sex,
        //        Handling = sel.Handling,
        //        Position = sel.Position,
        //        Period_of_nutrition = sel.Period_of_nutrition,
        //        Number_of_contracts = sel.Number_of_contracts,
        //        Number_of_category = sel.Number_of_category
        //    });
        //}

        public void DelRowById(int id)
        {
            _staffRepository.DeleteRow(id);
        }

        public void EditStaff(int id, StaffDataModel model)
        {
            Staff staff = new Staff();
            staff.Number_of_staff = id;
            staff.Name = model.Name;
            staff.Surname = model.Surname;
            staff.Middle_name = model.Middle_name;
            staff.Birthday = model.Birthday;
            staff.Employment_day = model.Employment_day;
            staff.Addresses = staff.Addresses;
            staff.Phone_number = model.Phone_number;
            staff.Name_of_position = model.Name_of_position;
            _staffRepository.EditRow(staff);
        }
    }
}
