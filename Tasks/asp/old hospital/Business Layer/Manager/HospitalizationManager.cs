﻿using Business_Layer.DataModels;
using Data_Access_Layer;
using Data_Access_Layer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer.Manager
{
   public  class HospitalizationManager
    {
        private HospitalizationRepository _patientRepository = new HospitalizationRepository();
 

        public void AddHospitalization(int? id,HospitalizationDataModel newhospitalisationmodel)
        {
            Hospitalization newhospitalisation = new Hospitalization();
            newhospitalisation.Number_of_hospitalization = newhospitalisationmodel.Number_of_hospitalization;
            newhospitalisation.Handling = newhospitalisationmodel.Handling;
            newhospitalisation.Directional = newhospitalisationmodel.Directional;
            newhospitalisation.Diagnosis_of_directional = newhospitalisationmodel.Diagnosis_of_directional;
            newhospitalisation.Through_hours = newhospitalisationmodel.Through_hours;
            newhospitalisation.Diagnosis_of_clinical = newhospitalisationmodel.Diagnosis_of_clinical;
            newhospitalisation.Diagnosis_of_conclusive = newhospitalisationmodel.Diagnosis_of_conclusive;
            newhospitalisation.Diagnosis_of_hospitalization = newhospitalisationmodel.Diagnosis_of_hospitalization;
            newhospitalisation.Date_of_hospitalization = newhospitalisationmodel.Date_of_hospitalization;
            newhospitalisation.Number_of_med_cards = id;
            newhospitalisation.Reasons = newhospitalisationmodel.Reasons;
            _patientRepository.AddHospitalization(newhospitalisation);
        }

        public IEnumerable<HospitalizationDataModel> GetListOfHospitalization()
        {
            return _patientRepository.GetList().Select(sel => new HospitalizationDataModel
            {
                //Name=sel.Name,
                //Surname=sel.Surname,
                //Middle_name=sel.Middle_name,
                Number_of_hospitalization = sel.Number_of_hospitalization,
                Handling = sel.Handling,
                Directional=sel.Directional,
                Diagnosis_of_directional=sel.Diagnosis_of_directional,
                Through_hours=sel.Through_hours,
                Diagnosis_of_clinical=sel.Diagnosis_of_clinical,
                Diagnosis_of_conclusive=sel.Diagnosis_of_conclusive,
                Diagnosis_of_hospitalization=sel.Diagnosis_of_hospitalization,
                Date_of_hospitalization=sel.Date_of_hospitalization,
                Number_of_med_cards=sel.Number_of_med_cards,
                Reasons=sel.Reasons
            });
        }

        public IEnumerable<HospitalizationWithPationtsDataModel> GetListOfHospitalizationWithPaz()
        {
            return _patientRepository.GetListWithPaz().Select(sel => new HospitalizationWithPationtsDataModel
            {
                Name = sel.Name,
                Surname = sel.Surname,
                Middle_name = sel.Middle_name,
                Number_of_hospitalization = sel.Number_of_hospitalization,
                Handling = sel.Handling,
                Directional = sel.Directional,
                Diagnosis_of_directional = sel.Diagnosis_of_directional,
                Through_hours = sel.Through_hours,
                Diagnosis_of_clinical = sel.Diagnosis_of_clinical,
                Diagnosis_of_conclusive = sel.Diagnosis_of_conclusive,
                Diagnosis_of_hospitalization = sel.Diagnosis_of_hospitalization,
                Date_of_hospitalization = sel.Date_of_hospitalization,
                Number_of_med_cards = sel.Number_of_med_cards,
                Reasons = sel.Reasons
            });
        }

        public void DelRowById(int id)
        {
            _patientRepository.DeleteRow(id);
        }

        public void EditPatientcategory(int id, HospitalizationDataModel sel)
        {
            Hospitalization newhospitalization = new Hospitalization();
            newhospitalization.Number_of_hospitalization = id;
            newhospitalization.Handling = sel.Handling;
            newhospitalization.Directional = sel.Directional;
            newhospitalization.Diagnosis_of_directional = sel.Diagnosis_of_directional;
            newhospitalization.Through_hours = sel.Through_hours;
            newhospitalization.Diagnosis_of_clinical = sel.Diagnosis_of_clinical;
            newhospitalization.Diagnosis_of_conclusive = sel.Diagnosis_of_conclusive;
            newhospitalization.Diagnosis_of_hospitalization = sel.Diagnosis_of_hospitalization;
            newhospitalization.Date_of_hospitalization = sel.Date_of_hospitalization;
            newhospitalization.Number_of_med_cards = sel.Number_of_med_cards;
            newhospitalization.Reasons = sel.Reasons;
            _patientRepository.EditRow(newhospitalization);
        }
    }
}
