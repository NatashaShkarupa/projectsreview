﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Access_Layer;
using Data_Access_Layer.Contractss;
using Data_Access_Layer.Repositories;
using Business_Layer.DataModels;

namespace Business_Layer.Manager
{
    public class CompartmentManager
    {
        private CompartmentRepository _comparementRepository = new CompartmentRepository();

        public IEnumerable<CompartmentDataModel> GetListOfCompartment()
        {
            return _comparementRepository.GetList().Select(sel => new CompartmentDataModel
            {
                Number_of_compartment = sel.Number_of_compartment,
                Name_of_compartment = sel.Name_of_compartment
            });
        }

        public void AddCompartment(CompartmentDataModel sel)
        {
            Compartment newpatient = new Compartment();
            newpatient.Number_of_compartment = sel.Number_of_compartment;
            newpatient.Name_of_compartment = sel.Name_of_compartment;
            _comparementRepository.AddCompartment(newpatient);
            _comparementRepository.Save();
        }

        public IEnumerable<CompartmentDataModel> GetListOfCompartmentbyword(string word)
        {
            return _comparementRepository.GetList(x => x.Name_of_compartment.Contains(word)).Select(sel => new CompartmentDataModel
            {
                Number_of_compartment = sel.Number_of_compartment,
                Name_of_compartment = sel.Name_of_compartment
            });
        }

        public void DelRowById(int id)
        {
            _comparementRepository.DeleteRow(id);
        }
        public void EditCompartment(int id, CompartmentDataModel patientmodel)
        {
            Compartment patient = new Compartment();
            patient.Number_of_compartment = id;
            patient.Name_of_compartment = patientmodel.Name_of_compartment;
            _comparementRepository.EditRow(patient);
        }
    }
}
