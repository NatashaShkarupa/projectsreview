﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Access_Layer;
using Data_Access_Layer.Contractss;
using Data_Access_Layer.Repositories;
using Business_Layer.DataModels;
using System.Globalization;

namespace Business_Layer.Manager
{
    public class PatientManager
    {
        private PatientRepozitories _patientRepository = new PatientRepozitories();

        public IEnumerable<PatientDataModel> GetListOfPatient()
        {
            return _patientRepository.GetList().Select(sel => new PatientDataModel
            {
                Number_of_med_cards = sel.Number_of_med_cards,
                Name = sel.Name,
                Surname = sel.Surname,
                Middle_name = sel.Middle_name,
                Birthday = sel.Birthday,
                Sex =sel.Sex,
                Position = sel.Position,
                Period_of_nutrition=sel.Period_of_nutrition,
                Number_of_contracts=sel.Number_of_contracts,
                Number_of_category=sel.Number_of_category
        });
        }

        public void AddPatient(PatientDataModel newpatientmodel)
        {
               Patient newpatient = new Patient();
                newpatient.Name = newpatientmodel.Name;
                newpatient.Surname = newpatientmodel.Surname;
                newpatient.Middle_name = newpatientmodel.Middle_name;
                newpatient.Birthday = newpatientmodel.Birthday;
                newpatient.Sex = newpatientmodel.Sex;
                newpatient.Position = newpatientmodel.Position;
                newpatient.Period_of_nutrition = newpatientmodel.Period_of_nutrition;
                newpatient.Number_of_contracts = newpatientmodel.Number_of_contracts;

                newpatient.Number_of_category = newpatientmodel.Number_of_category;

                _patientRepository.AddBook(newpatient);
                _patientRepository.Save();
        }

        public IEnumerable<PatientDataModel> GetListOfBooksByWord(string word)
        {
            return _patientRepository.GetList(x => x.Name.Contains(word)).Select(sel => new PatientDataModel
            {
                Number_of_med_cards = sel.Number_of_med_cards,
                Name = sel.Name,
                Surname = sel.Surname,
                Middle_name = sel.Middle_name,
                Birthday = sel.Birthday,
                Sex = sel.Sex,
                Position = sel.Position,
                Period_of_nutrition = sel.Period_of_nutrition,
                Number_of_contracts = sel.Number_of_contracts,
                Number_of_category = sel.Number_of_category
            });
        }

        //public IEnumerable<PatientDataModel> FindRowById(int? id)
        //{
        //    return _patientRepository.GetList(x => x.Number_of_med_cards==id).Select(sel => new PatientDataModel
        //    {
        //        Number_of_med_cards = sel.Number_of_med_cards,
        //        Name = sel.Name,
        //        Surname = sel.Surname,
        //        Middle_name = sel.Middle_name,
        //        Birthday = sel.Birthday,
        //        Sex = sel.Sex,
        //        Handling = sel.Handling,
        //        Position = sel.Position,
        //        Period_of_nutrition = sel.Period_of_nutrition,
        //        Number_of_contracts = sel.Number_of_contracts,
        //        Number_of_category = sel.Number_of_category
        //    });
        //}

        public void DelRowById(int id)
        {
            _patientRepository.DeleteRow(id);
        }

        public void EditPatient(int id, PatientDataModel patientmodel)
        {
            Patient patient = new Patient();
            patient.Number_of_med_cards = id;
                patient.Name = patientmodel.Name;
                patient.Surname = patientmodel.Surname;
                patient.Middle_name = patientmodel.Middle_name;
                patient.Birthday = patientmodel.Birthday;
                patient.Sex = patientmodel.Sex;
                patient.Position = patientmodel.Position;
                patient.Period_of_nutrition = patientmodel.Period_of_nutrition;
                patient.Number_of_contracts = patientmodel.Number_of_contracts;
                patient.Number_of_category = patientmodel.Number_of_category;
                _patientRepository.EditRow(patient);
        }
    }
}
