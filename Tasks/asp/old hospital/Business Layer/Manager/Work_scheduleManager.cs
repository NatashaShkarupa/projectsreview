﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data_Access_Layer;
using Data_Access_Layer.Contractss;
using Data_Access_Layer.Repositories;
using Business_Layer.DataModels;
using System.Globalization;

namespace Business_Layer.Manager
{
    public class Work_scheduleManager
    {
        private WorkSchedulerRepository _Repository = new WorkSchedulerRepository();

        public IEnumerable<Work_scheduleDataModel> GetListOfWork_Sheduler()
        {
            return _Repository.GetList().Select(sel => new Work_scheduleDataModel
            {
                Number_of_work_schedule = sel.Number_of_work_schedule,
                Data_of_work = sel.Date_of_work,
                Time_of_beginning = sel.Time_of_work_beginning,
                Time_of_ending = sel.Time_of_work_ending,
                Number_of_staff = sel.Number_of_staff
            });
        }

        public IEnumerable<Work_scheduleWithStaffDataModel> GetListOfWork_ShedulerWithStaff()
        {
            return _Repository.GetListWithStaff().Select(sel => new Work_scheduleWithStaffDataModel
            {
                Number_of_work_schedule=sel.Number_of_work_schedule,
                Name = sel.Name,
                Surname = sel.Surname,
                Middle_name = sel.Middle_name,
                Data_of_work = sel.Data_of_work,
                Time_of_beginning = sel.Time_of_beginning,
                Time_of_ending = sel.Time_of_ending
            });
        }

        public void AddWork_Sheduler(Work_scheduleDataModel newpatientmodel)
        {
            Work_schedule newpatient = new Work_schedule();
            newpatient.Number_of_work_schedule = newpatientmodel.Number_of_work_schedule;
            newpatient.Date_of_work = newpatientmodel.Data_of_work;
            newpatient.Time_of_work_beginning = newpatientmodel.Time_of_beginning;
            newpatient.Time_of_work_ending = newpatientmodel.Time_of_ending;
            newpatient.Number_of_staff = newpatientmodel.Number_of_staff;
            _Repository.AddWork_schedule(newpatient);
            _Repository.Save();
        }

        public void DelRowById(int id)
        {
            _Repository.DeleteRow(id);
        }

        public void EditPatient(int id, Work_scheduleDataModel newpatientmodel)
        {
            Work_schedule newpatient = new Work_schedule();
            newpatient.Number_of_work_schedule = id;
            newpatient.Date_of_work = newpatientmodel.Data_of_work;
            newpatient.Time_of_work_beginning = newpatientmodel.Time_of_beginning;
            newpatient.Time_of_work_ending = newpatientmodel.Time_of_ending;
            newpatient.Number_of_staff = newpatientmodel.Number_of_staff;
            _Repository.EditRow(newpatient);
        }
    }
}
