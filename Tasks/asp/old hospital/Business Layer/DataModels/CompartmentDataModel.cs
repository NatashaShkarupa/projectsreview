﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class CompartmentDataModel
    {
        public int Number_of_compartment { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть назву відділення", AllowEmptyStrings = false)]
        public string Name_of_compartment { get; set; }
    }
}
