﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class ReviewDataModel
    {
        public int Number_of_review { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть результат огляду на педикульозу", AllowEmptyStrings = false)]
        public string Pediculosis { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату аналізу на педикульозу", AllowEmptyStrings = false)]
        public DateTime DatePediculosis { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть результат огляду на коросту", AllowEmptyStrings = false)]
        public string Scabies { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату аналізу на коросту", AllowEmptyStrings = false)]
        public DateTime DateScabies { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть результат огляду на ВІЛ", AllowEmptyStrings = false)]
        public string HIV { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату аналізу на ВІЛ", AllowEmptyStrings = false)]
        public DateTime DateHIV { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть результат огляду на гепатит", AllowEmptyStrings = false)]
        public string Hepatitis { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату аналізу на гепатит", AllowEmptyStrings = false)]
        public DateTime DateHepatitis { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть результат огляду на RW", AllowEmptyStrings = false)]
        public string RW { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату аналізу на RW", AllowEmptyStrings = false)]
        public DateTime DateRW { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть пульс", AllowEmptyStrings = false)]
        public double Pulse { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть тиск(систолітичний)", AllowEmptyStrings = false)]
        public double Systolic_pressure { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть тиск(діалістичний)", AllowEmptyStrings = false)]
        public double Ciastolic_pressure { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть чутливість до препаратів", AllowEmptyStrings = false)]
        public string Sensitivity { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть групу крові", AllowEmptyStrings = false)]
        public int Blood_type { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть резус фактор крові", AllowEmptyStrings = false)]
        public string Rh_factor { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату аналізу на крупу крові", AllowEmptyStrings = false)]
        public DateTime Date_blood { get; set; }
        public int Number_of_med_cards { get; set; }
    }
}
