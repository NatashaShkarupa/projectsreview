﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer.DataModels
{
    public class StaffDataModel
    {
        public int Number_of_staff { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть ім'я", AllowEmptyStrings = false)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть прізвище", AllowEmptyStrings = false)]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть по батькові", AllowEmptyStrings = false)]
        public string Middle_name { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd'.'MM'.'yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Будь ласка виберіть дату народження", AllowEmptyStrings = false)]
        public DateTime Birthday { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd'.'MM'.'yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Будь ласка виберіть дату прийняття на роботу", AllowEmptyStrings = false)]
        public DateTime Employment_day { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть адресу", AllowEmptyStrings = false)]
        public string Addresses { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть номер телефону", AllowEmptyStrings = false)]
        public long Phone_number { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть посаду", AllowEmptyStrings = false)]
        public string Name_of_position { get; set; }
    }
}
