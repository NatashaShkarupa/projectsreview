﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class UserssDataModel
    {
        public int Number_of_users { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть логін", AllowEmptyStrings = false)]
        public string Logins { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть пароль", AllowEmptyStrings = false)]
        public string Passwords { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть роль", AllowEmptyStrings = false)]
        public string Roles { get; set; }
    }
}
