﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer.DataModels
{
    public class ViewModel
    {
            public IEnumerable<ContractsDataModel> Contract { get; set; }
            public IEnumerable<Category_PatientsDataModel> Category { get; set; }
            public IEnumerable<CompartmentDataModel> Compartment { get; set; }
    }
}
