﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class HospitalizationDataModel
    {
        public int Number_of_hospitalization { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть частоту звернення", AllowEmptyStrings = false)]
        public string Handling { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть ким направлений", AllowEmptyStrings = false)]
        public string Directional { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть діагноз закладу який направив", AllowEmptyStrings = false)]
        public string Diagnosis_of_directional { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть через який час", AllowEmptyStrings = false)]
        public int Through_hours { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть діагноз клінічний", AllowEmptyStrings = false)]
        public string Diagnosis_of_clinical { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть діагноз встановлений", AllowEmptyStrings = false)]
        public string Diagnosis_of_conclusive { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть діагноз при госпіталізації", AllowEmptyStrings = false)]
        public string Diagnosis_of_hospitalization { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату госпіталізації", AllowEmptyStrings = false)]
        [DisplayFormat(DataFormatString = "{0:dd'.'MM'.'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date_of_hospitalization { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть причину госпіталізації", AllowEmptyStrings = false)]
        public string Reasons { get; set; }
        public int? Number_of_med_cards { get; set; }
    }
}
