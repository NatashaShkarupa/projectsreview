﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class ContractsDataModel
    {
        //[Required(ErrorMessage = "Будь ласка введіть номер контракта", AllowEmptyStrings = false)]
        public string Number_of_contracts { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть назву компанії", AllowEmptyStrings = false)]
        public string Company_name { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть тип контракту", AllowEmptyStrings = false)]
        public string Scope_of_the_contracts { get; set; }
    }
}
