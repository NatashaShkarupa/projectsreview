﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class Work_scheduleDataModel
    {
        public int Number_of_work_schedule { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть дату роботи", AllowEmptyStrings = false)]
        [DisplayFormat(DataFormatString = "{0:dd'.'MM'.'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Data_of_work { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть час початку робочого дня", AllowEmptyStrings = false)]
        public TimeSpan Time_of_beginning { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть час закінчення робочого дня", AllowEmptyStrings = false)]
        public TimeSpan Time_of_ending { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть працівника", AllowEmptyStrings = false)]
        public int? Number_of_staff{ get; set; }
    }
}
