﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class Place_of_residenceDataModel
    {
        public int Number_of_med_cards { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть проживання", AllowEmptyStrings = false)]
        public int Residence { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть область", AllowEmptyStrings = false)]
        public string Region { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть район", AllowEmptyStrings = false)]
        public int District { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть населений пункт", AllowEmptyStrings = false)]
        public int Town { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть адресу", AllowEmptyStrings = false)]
        public string Addresses { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть номер телефону", AllowEmptyStrings = false)]
        public int Phone_number { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть місце проживання", AllowEmptyStrings = false)]
        public string Place_of_residence { get; set; }
    }
}
