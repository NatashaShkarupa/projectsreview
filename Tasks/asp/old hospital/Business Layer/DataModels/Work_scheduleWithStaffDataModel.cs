﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Layer.DataModels
{
    public class Work_scheduleWithStaffDataModel
    {
        public int Number_of_work_schedule { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Middle_name { get; set; }
        public DateTime Data_of_work { get; set; }
        public TimeSpan Time_of_beginning { get; set; }
        public TimeSpan Time_of_ending { get; set; }
    }
}
