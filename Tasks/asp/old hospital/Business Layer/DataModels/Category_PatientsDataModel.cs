﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class Category_PatientsDataModel
    {
        public int Number_of_category{ get; set; }
        [Required(ErrorMessage = "Будь ласка введіть тип категорії", AllowEmptyStrings = false)]
        public string Category_type { get; set; }
    }
}
