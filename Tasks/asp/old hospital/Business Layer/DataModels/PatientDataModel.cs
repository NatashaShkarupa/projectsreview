﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Business_Layer.DataModels
{
    public class PatientDataModel
    {
        public int Number_of_med_cards { get; set; }
        [StringLength(30, MinimumLength = 3, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Name", ResourceType = typeof(Resources.Resource))]
        public string Name { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть прізвище", AllowEmptyStrings = false)]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Будь ласка введіть по батькові", AllowEmptyStrings = false)]
        public string Middle_name { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть стать", AllowEmptyStrings = false)]
        public string Sex { get; set; }
        //[DataType(DataType.Date)]

        [DisplayFormat(DataFormatString = "{0:dd'.'MM'.'yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Будь ласка виберіть дату народження", AllowEmptyStrings = false)]
        public DateTime Birthday { get; set; }

        [Required(ErrorMessage = "Будь ласка введіть посаду", AllowEmptyStrings = false)]
        public string Position { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть харчування", AllowEmptyStrings = false)]
        public string Period_of_nutrition { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть контракт", AllowEmptyStrings = false)]
        public string Number_of_contracts { get; set; }
        [Required(ErrorMessage = "Будь ласка виберіть категорію", AllowEmptyStrings = false)]
        public int? Number_of_category { get; set; }
    }
}
