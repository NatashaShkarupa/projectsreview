﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Calculator.Models
{
    public class ProductsViewModel
    {

        [Required]
        [Display(Name = "номер")]
        public int id { get; set; }
        [Required]
        [Display(Name = "Наименование продукта")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Покупатель")]
        public string Customer { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public double Price { get; set; }
    }
}