﻿using Calculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Calculator.Controllers
{
    public class ProductsController : Controller
    {
        Viewmodel model = new Viewmodel();
        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult Create(ProductsViewModel _productsList)
        {
            try
            {
                model.Products.Add(_productsList);
                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        //// GET: Products/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Products/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, ProductsViewModel _productsList)
        //{
        //    try
        //    {
        //        _productsList.Products.Where(p => p.id == id).FirstOrDefault{
        //            id=
        //        };
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Products/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ProductsViewModel _productsList)
        {
            try
            {
                model.Products.RemoveAt(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        //public ActionResult List()
        //{
        //    IEnumerable<Viewmodel> models ;
        //    return View(models);
        //}
    }
}
