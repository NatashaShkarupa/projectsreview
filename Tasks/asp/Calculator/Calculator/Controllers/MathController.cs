﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CalculationLibrary;

namespace Calculator.Controllers
{
    public class MathController : Controller
    {
        private ClasicCalculator _calculator=new ClasicCalculator();

        public ActionResult Add(double firstNumber, double secondNumber)
        {
            double result = _calculator.AddOperation(firstNumber, secondNumber);
            ViewBag.Result = result;
            return View();
        }

        public ActionResult Sub(double firstNumber, double secondNumber)
        {
            double result = _calculator.AddOperation(firstNumber, secondNumber);
            ViewBag.Result = result;
            return View();
        }

        public ActionResult Mul(double firstNumber, double secondNumber)
        {
            double result = _calculator.AddOperation(firstNumber, secondNumber);
            ViewBag.Result = result;
            return View();
        }

        public ActionResult Div(double firstNumber, double secondNumber)
        {
            double result = _calculator.AddOperation(firstNumber, secondNumber);
            ViewBag.Result = result;

            return View();
        }
    }
}