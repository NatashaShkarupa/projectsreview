﻿app.controller('mainController', function ($scope) {

    $scope.headerContent = {
        title: 'Registration'
    };

    $scope.list = model;

    $scope.addUser = function (name, surname, email, password, number, age) {
        number = parseFloat(number); // преобразуем введенное значение к числу
        if (number != "" && !isNaN(number)) // если текст установлен и введено число, то добавляем
        {
            $scope.list.items.push({ name: name, surname: surname, email: email, password: password, number: number, age: false });
        }
    }

    $scope.deleteUser = function (item) {
        var index = $scope.list.items.indexOf(item);
        $scope.list.items.splice(index, 1);
    }

    // $scope.updateUser = function (item) {
    //     var index = $scope.list.items.indexOf(item).age;
    //     item.age=
    // }
});

var model = {
    items: [
        {
            name: "Natasha",
            surname: "Shkarupa",
            email: "shkarupa@mail.ru",
            password: "shkarupa96",
            number: 06649004,
            age: false
        },
        {
            name: "John",
            surname: "Pettersone",
            email: "john78@mail.ru",
            password: "john78",
            number: 066904504,
            age: true
        }
    ]
};