﻿app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        .state('home', {
            url: '/home',
            templateUrl: 'View/Home.html'
        })
        
        // nested list with custom controller
        .state('home.registration', {
            url: '/registration',
            templateUrl: 'View/Registration.html',
            controller: 'mainController'
        })
        
        .state('home.about', {
            url: '/about',
            template: 'I could sure use a drink right now.'
        })
        
        .state('about', {
            url: '/about',
            views: {
                '': { templateUrl: 'View/About.html' },
                'columnOne@about': { template: 'Look I am a column!' },
                'columnTwo@about': { 
                    template: 'Look I am a column!' 
                }
            }
            
    });
        
});
