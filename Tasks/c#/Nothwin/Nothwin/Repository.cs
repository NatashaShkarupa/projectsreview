﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nothwin
{
    public class Repository : BaseRepository
    {
        public void Task18()
        {
            using (var context = GetContext())
            {
                var query = (from p in context.Products
                             join
                             od in context.Order_Details
                             on p.ProductID equals od.ProductID
                             where (od.Quantity > 10)
                             orderby p.ProductName
                             select new { p.ProductName, od.Quantity }
                ).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine(a.ProductName, a.Quantity);
                }
            }
        }


        public void Task19()
        {
            using (var context = GetContext())
            {
                var query = (from c in context.Customers
                             join
                             o in context.Orders
                             on c.CustomerID equals o.CustomerID
                             join
                             od in context.Order_Details
                             on o.OrderID equals od.OrderID
                             where (od.Quantity > 5)
                             orderby od.UnitPrice
                             select new { c.ContactName, od.Quantity }
                ).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine(a.ContactName, a.Quantity);
                }
            }
        }


        public void Task20()
        {
            using (var context = GetContext())
            {
                var query = (from c in context.Customers
                             join
                             o in context.Orders
                             on c.CustomerID equals o.CustomerID
                             where c.ContactName.Contains("a")
                             group c by c.ContactName into grouped
                             select new
                             {
                                 ContactName = grouped.Key,
                                 count = grouped.Count(t=>t.CustomerID!=null)
                             }
                ).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1}", a.ContactName, a.count);
                }
            }
        }

        public void Task21()
        {
            using (var context = GetContext())
            {
                IQueryable<Product_Sales_for_1997> query = context.Product_Sales_for_1997.Select(x => x);

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1} {2}", a.ProductName, a.ProductSales, a.CategoryName);
                }
            }
        }

        public void Task21Two()
        {
            using (var context = GetContext())
            {
                DateTime fromDate = Convert.ToDateTime("1997-01-01");
                var query = (from o in context.Orders
                             join
                             od in context.Order_Details
                             on o.OrderID equals od.OrderID
                             join
                             p in context.Products
                             on od.ProductID equals p.ProductID
                             where o.OrderDate < fromDate
                             orderby od.UnitPrice
                             select new { p.ProductName, od.Quantity, od.UnitPrice, o.OrderDate }).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1} {2} {3}", a.ProductName, a.Quantity, a.UnitPrice, a.OrderDate.GetValueOrDefault().ToString("dd/M/yyyy", CultureInfo.InvariantCulture));
                }
            }
        }

        public void Task22()
        {
            using (var context = GetContext())
            {
                var query = (from c in context.Customers
                             join
                             o in context.Orders
                             on c.CustomerID equals o.CustomerID
                             join
                             od in context.Order_Details
                             on o.OrderID equals od.OrderID
                             join 
                             p in context.Products
                             on od.ProductID equals p.ProductID
                             orderby od.UnitPrice
                             select new { p.ProductName, od.Quantity, o.OrderDate, c.ContactName }
                ).Take(10).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1} {2} {3}", a.ProductName, a.Quantity, a.OrderDate.GetValueOrDefault().ToString("dd/M/yyyy", CultureInfo.InvariantCulture), a.ContactName);
                }
            }
        }

        public void Task23()
        {
            using (var context = GetContext())
            {
                var query = (from c in context.Customers
                             join
                             o in context.Orders
                             on c.CustomerID equals o.CustomerID
                             join
                             od in context.Order_Details
                             on o.OrderID equals od.OrderID
                             join
                             p in context.Products
                             on od.ProductID equals p.ProductID
                             orderby od.UnitPrice
                             group od by p.ProductName into grouped
                             select new { ProductName=grouped.Key, sum= grouped.Sum(t=>t.UnitPrice*t.Quantity) }
                ).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1}", a.ProductName, a.sum);
                }
            }
        }

    public void Task24()
        {
            using (var context = GetContext())
            {
                IQueryable<Summary_of_Sales_by_Quarter> query = context.Summary_of_Sales_by_Quarter.Select(x => x);

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1}", a.Subtotal, a.ShippedDate);
                }
            }
        }

        public void Task25()
        {
            using (var context = GetContext())
            {
                DateTime fromDate = Convert.ToDateTime("1997-01-01");
                DateTime toDate = Convert.ToDateTime("1997-12-31");
                var query = (from c in context.Customers
                             join
                             o in context.Orders
                             on c.CustomerID equals o.CustomerID
                             join
                             od in context.Order_Details
                             on o.OrderID equals od.OrderID
                             join
                             p in context.Products
                             on od.ProductID equals p.ProductID
                             orderby od.UnitPrice
                             where  o.OrderDate > fromDate && o.OrderDate< toDate
                             group od by p.ProductName into grouped
                             select new { ProductName = grouped.Key, sum = grouped.Sum(t => t.UnitPrice * t.Quantity) }
                ).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1}", a.ProductName, a.sum);
                }
            }
        }

        public void Task26()
        {
            using (var context = GetContext())
            {
                DateTime fromDate = Convert.ToDateTime("1997-01-01");
                DateTime toDate = Convert.ToDateTime("1997-12-31");
                var query = (from c in context.Customers
                             join
                             o in context.Orders
                             on c.CustomerID equals o.CustomerID
                             join
                             od in context.Order_Details
                             on o.OrderID equals od.OrderID
                             join
                             p in context.Products
                             on od.ProductID equals p.ProductID
                             orderby od.UnitPrice
                             where o.OrderDate > fromDate && o.OrderDate < toDate
                             group od by p.ProductName into grouped
                             select new { ProductName = grouped.Key, sum = grouped.Sum(t => t.UnitPrice * t.Quantity) }
                ).Take(8).ToList();

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1}", a.ProductName, a.sum);
                }
            }
        }

        public void Task27()
        {
            using (var context = GetContext())
            {
                IQueryable<Sales_Totals_by_Amount> query = context.Sales_Totals_by_Amount.Where(t=>t.SaleAmount>500).Select(x => x);

                foreach (var a in query)
                {
                    Console.WriteLine("{0}-{1}", a.CompanyName, a.SaleAmount);
                }
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }
    }
}