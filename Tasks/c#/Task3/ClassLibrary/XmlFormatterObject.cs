﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ClassLibrary3
{
    public class XmlFormatterObject
    {
        private XmlSerializer _formatter;

        public void ObjectSerialize(Persone someObject)
        {
            using (FileStream fs = new FileStream("people.xml", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
            }
        }

        public void ObjectSerialize(Teacher someObject)
        {
            using (FileStream fs = new FileStream("teacher.xml", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
            }
        }

        public void ObjectSerialize(List<Student> someObject)
        {
            using (FileStream fs = new FileStream("student.xml", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
            }
        }

        public void ObjectDeserialize(Persone someObject)
        {
            using (FileStream fs = new FileStream("persone.xml", FileMode.OpenOrCreate))
            {
                Persone[] newpeople = (Persone[])_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }

        public void ObjectDeserialize(Teacher someObject)
        {
            using (FileStream fs = new FileStream("teacher.xml", FileMode.OpenOrCreate))
            {
                Teacher[] newpeople = (Teacher[])_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }

        public void ObjectDeserialize(Student someObject)
        {
            using (FileStream fs = new FileStream("student.xml", FileMode.OpenOrCreate))
            {
                Student[] newpeople = (Student[])_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }
    }
}
