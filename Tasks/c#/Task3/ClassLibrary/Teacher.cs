﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3
{
    [Serializable]
    public class Teacher : Persone
    {
        public string SubjectName { get; set; }
        public string SubjectBook { get; set; }
        public int IdTeacher { get; set; }

        public Teacher(int idTeacher, string subjectName, string subjectBook, int idPersone, string firstName, string secondName,
            string address)
            : base(idPersone, firstName, secondName, address)
        {
            this.IdTeacher = idTeacher;
            this.SubjectName = subjectName;
            this.SubjectBook = subjectBook;
        }


        public override string ToString()
        {
            return "Students: " + IdTeacher + " " + FirstName + " " + SecondName + " - " + SubjectName + " " + SubjectBook;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            Teacher objAsPart = obj as Teacher;
            if (objAsPart == null)
            {
                return false;
            }
            else return Equals(objAsPart);
        }

        public override int GetHashCode()
        {
            return IdTeacher;
        }

        public bool Equals(Teacher other)
        {
            if (other == null) return false;
            return (this.IdTeacher.Equals(other.IdTeacher));
        }

        private List<Student> studentList = new List<Student>();

        public void AddStudent(Student student)
        {
            studentList.Add(student);
        }


        public bool RemoveStudent(Student student)
        {
            //var test=list_student.Select(x => x == student);
            if (studentList.Exists(x => x == student))
            {
                return studentList.Remove(student);
            }
            else
            {
                return false;
            }
        }
    }
}
