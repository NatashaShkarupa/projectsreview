﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyExtensionMethods;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3;

namespace ClassLibrary3
{
    public class Persons: IEnumerable<Persone>
    {
        private ConsoleManager _consoleManager = new ConsoleManager();
        private List<Persone> _list=new List<Persone>();
  
        public IEnumerator<Persone> GetEnumerator()
        {
            return ((IEnumerable<Persone>)_list).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var l in _list)
            { 
                yield return l;
            }
        }

        public void Add(Persone persone)
        {
            _list.Add(persone);
        }

        public void ShowList()
        {
            foreach (var element in _list.ElementSort(x=>x.IdPersone))
            {
                _consoleManager.Messager(element.FirstName, element.Address, _list.IndexOf(element));
            }
        }

        public void ShowListDelegate()
        {
            foreach (var element in _list.ElementSort(((a, b) => a.FirstName.CompareTo(b.FirstName))))
            {
                _consoleManager.Messager(element.FirstName, element.Address, _list.IndexOf(element));
            }
        }
     
        // The count of elements in showing list
        public IEnumerable GetPersone(int max)
        {
            for (int i = 0; i < max; i++)
            {
                foreach (var l in _list)
                {
                    yield return l;
                }
            }
        }
    }
}
