﻿using System;
using System.Numerics;

namespace ClassLibrary3
{
    public struct ComplexNumber : IComparable<ComplexNumber>, IEquatable<ComplexNumber>
    {
        private double _real { get; set; }
        private double _imaginary { get; set; }

        public ComplexNumber(double real, double imaginary)
        {
            this._real = real;
            this._imaginary = imaginary;
        }

        public Complex ToComplex(double doubleValue)
        {
            Complex number = doubleValue;
            return number;
        }

        public override string ToString()
        {
            return (System.String.Format("{0} + {1}i", _real, _imaginary));
        }

        public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber(a._real + b._real, a._imaginary + b._imaginary);
        }

        public static ComplexNumber operator -(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber(a._real - b._real, a._imaginary - b._imaginary);
        }

        public static ComplexNumber operator *(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber(((a._real * b._real) - (a._imaginary * b._imaginary)), ((b._real * a._imaginary) + (a._real * b._imaginary)));
        }

        public static ComplexNumber operator /(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber(((a._real * a._imaginary) + (b._real * b._imaginary)) / ((a._imaginary * a._imaginary) + (b._imaginary * b._imaginary)), (((b._real * a._imaginary) - (a._real * b._imaginary)) / ((a._imaginary * a._imaginary) + (b._imaginary * b._imaginary))));
        }

        public override bool Equals(object obj)
        {
            if (obj is ComplexNumber)
            {
                ComplexNumber other = (ComplexNumber)obj;
                return obj.Equals(other);
            }
            else {
                return false;
            }
        }

        public bool Equals(ComplexNumber other)
        {
            return ((this._imaginary == other._imaginary) && (this._real < other._real));
        }

        public override int GetHashCode()
        {
            return (int)_real ^ (int)_imaginary; 
        }

        public static bool operator ==(ComplexNumber me, ComplexNumber other)
        {
            return me.Equals(other);
        }

        public static bool operator !=(ComplexNumber me, ComplexNumber other)
        {
            return !me.Equals(other);
        }

        public int CompareTo(object obj)
        {
            if (obj is ComplexNumber)
            {
                ComplexNumber other = (ComplexNumber)obj;
                return CompareTo(other);
            }
            else
            {
                throw new ArgumentException("o is not a ComplexNumber");
            }
        }

        public int CompareTo(ComplexNumber other)
        {
            if ((this._real == other._real) &&
                    (this._imaginary == other._imaginary))
            {
                return 0;
            }
            else if ((this._real < other._real) &&
                (this._imaginary < other._imaginary))
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }

        public static int operator >=(ComplexNumber me, ComplexNumber other)
        {
            return me.CompareTo(other);
        }

        public static int operator <=(ComplexNumber me, ComplexNumber other)
        {
            return me.CompareTo(other);
        }
    }
}
