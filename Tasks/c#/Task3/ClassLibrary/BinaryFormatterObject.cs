﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ClassLibrary3
{
    public class BinaryFormatterObject 
    {
        private BinaryFormatter _formatter = new BinaryFormatter();

        public void ObjectSerialize(Object someObject)
        {
            using (FileStream fs = new FileStream("people.dat", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
                Console.WriteLine("Объект сериализован");
            }
        }

        //public void ObjectSerialize(List<Teacher> someObject)
        //{
        //    using (FileStream fs = new FileStream("teacher.dat", FileMode.OpenOrCreate))
        //    {
        //        _formatter.Serialize(fs, someObject);
        //    }
        //}

        //public void ObjectSerialize(List<Student> someObject)
        //{
        //    using (FileStream fs = new FileStream("student.dat", FileMode.OpenOrCreate))
        //    {
        //        _formatter.Serialize(fs, someObject);
        //    }
        //}

        //public void ObjectDeserialize(List<Persone> someObject)
        //{
        //    using (FileStream fs = new FileStream("people.dat", FileMode.OpenOrCreate))
        //    {
        //        List<Persone> newPerson = (List<Persone>)_formatter.Deserialize(fs);
        //        Console.WriteLine("Объект десериализован");
        //    }
        //}

        //public void ObjectDeserialize(List<Teacher> someObject)
        //{
        //    using (FileStream fs = new FileStream("teacher.dat", FileMode.OpenOrCreate))
        //    {
        //        List<Teacher> newpeople = (List<Teacher>)_formatter.Deserialize(fs);
        //    }
        //    Console.ReadLine();
        //}

        public void ObjectDeserialize(List<Student> someObject)
        {
            using (FileStream fs = new FileStream("student.dat", FileMode.OpenOrCreate))
            {
                List<Student> newpeople = (List<Student>)_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }
    }
}
