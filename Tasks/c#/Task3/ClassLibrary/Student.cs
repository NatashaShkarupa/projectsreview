﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3
{
    public interface ListOperation
    {
        void AddTeacher(Teacher teacher);
        bool RemoveTeacher(Teacher teacher);
    }

    [Serializable]
    public class Student : Persone, IComparable<Student>, ListOperation/*, IEnumerable*/
    {
        public delegate void StudentEventHandler(string msg);
        public event StudentEventHandler StudentRaiseSomeEvent;

        public string NumberOfGroup { get; set; }
        public int ResultOfMathematicTest { get; set; }
        public int ResultOfInformaticTest { get; set; }
        public int NumberTeacher { get; set; }
        public int IdStudents { get; set; }

        public Student(int idStudents, string numberOfGroup, int resultOfMathematicTest, int resultOfInformaticTest, int numberTeacher,
            int idPersone, string firstName, string secondName, string address)
            : base(idPersone, firstName, secondName, address)
        {
            this.IdStudents = idStudents;
            this.NumberOfGroup = numberOfGroup;
            this.NumberTeacher = numberTeacher;
            this.ResultOfInformaticTest = resultOfInformaticTest;
            this.ResultOfMathematicTest = resultOfMathematicTest;
        }

        public override string ToString()
        {
            if (StudentRaiseSomeEvent != null)
                StudentRaiseSomeEvent("For this student was added the teacher");

            return "Students: " + IdStudents + " " + FirstName + " " + SecondName + " - " + NumberOfGroup
                + " " + ResultOfMathematicTest + " " + ResultOfInformaticTest + " " + NumberTeacher;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            Student objAsPart = obj as Student;
            if (objAsPart == null)
            {
                return false;
            }
            else return Equals(objAsPart);
        }

        public override int GetHashCode()
        {
            return IdStudents;
        }

        public bool Equals(Student other)
        {
            if (other == null) return false;
            return (this.IdStudents.Equals(other.IdStudents));
        }

        private List<Teacher> studentList = new List<Teacher>();

        public void AddTeacher(Teacher teacher)
        {
            studentList.Add(teacher);
        }

        public bool RemoveTeacher(Teacher teacher)
        {
            if (studentList.Exists(x => x == teacher))
            {
                return studentList.Remove(teacher);
            }
            else
            {
                return false;
            }
        }

        public int CompareTo(Student other)
        {
            return this.IdPersone.CompareTo(other.IdPersone);
        }

       static bool SortByNumberOfGroup
        {
            get; set;
        }
        static bool SortByResultOfMathematicTest
        {
            get; set;
        }
        static bool SortByResultOfInformaticTest
        {
            get; set;
        }

        public class StudentComparer : IComparer<Student>
        {
            public StudentComparer(bool SortByNumberOfGroup, bool SortByResultOfMathematicTest, bool sortByResultOfInformaticTest)
            {
                SortByNumberOfGroup = Student.SortByNumberOfGroup;
                SortByResultOfMathematicTest = Student.SortByResultOfMathematicTest;
                sortByResultOfInformaticTest = Student.SortByResultOfInformaticTest;
            }

            public int Compare(Student x, Student y)
            {
                if (Student.SortByNumberOfGroup==true)
                {
                    return x.ResultOfMathematicTest.CompareTo(y.ResultOfMathematicTest);
                }
                else if (Student.SortByResultOfMathematicTest == true)
                {
                    return x.NumberOfGroup.CompareTo(y.NumberOfGroup);
                }
                else if (Student.SortByResultOfInformaticTest == true)
                {
                    return x.ResultOfInformaticTest.CompareTo(y.ResultOfInformaticTest);
                }
                else
                {
                    return 0;
                }
            }

        }
    }
}
