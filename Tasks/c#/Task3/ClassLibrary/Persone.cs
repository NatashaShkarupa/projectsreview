﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3
{
    [Serializable]
    public class Persone : ICloneable
    {
        public delegate void PersoneListStateHandler(string msg);
        public event PersoneListStateHandler PersoneAddEvent;

        public int IdPersone { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Address { get; set; }
        public int NumberOfSchool { get; set; }
        ConsoleManager consoleManager = new ConsoleManager();

        public Persone(int idPersone, string firstName, string secondName, string address)
        {
            this.IdPersone = idPersone;
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.Address = address;
        }

        virtual public void Print()
        {
            if (PersoneAddEvent!=null)
                PersoneAddEvent("The list op people was adding");
            consoleManager.Messager(IdPersone, FirstName, SecondName, Address);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            Persone objAsPart = obj as Persone;
            if (objAsPart == null)
            {
                return false;
            }
            else return Equals(objAsPart);
        }

        public override int GetHashCode()
        {
            return IdPersone;
        }

        public bool Equals(Persone other)
        {
            if (other == null) return false;
            return (this.IdPersone.Equals(other.IdPersone));
        }

        /// <summary>
        ///  Shallow copys.
        /// </summary>
        //public object Clone()
        //{
        //    return this.MemberwiseClone();
        //}

        /// <summary>
        ///  Deep copy.
        /// </summary>
        public object Clone()
        {
            return new Persone(IdPersone = this.IdPersone, FirstName = this.FirstName, SecondName = this.SecondName, Address = this.Address);
        }
    }
}
