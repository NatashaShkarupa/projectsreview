﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary3
{
    [Serializable]
    public class ConsoleManager
    {
        public int PromptForInt32()
        {
            while (true)
            {
                int result;
                if (Int32.TryParse(Console.ReadLine(), out result))
                {
                    return result;
                }
                Console.WriteLine("Please, enter the number.");
            }
        }

        public double PromptForDouble()
        {
            while (true)
            {
                double result;
                if (double.TryParse(Console.ReadLine(), out result))
                {
                    return result;
                }
                Console.WriteLine("Please, enter the number.");
            }
        }

        public DateTime PromptForDateTime()
        {
            while (true)
            {
                DateTime result;
                if (DateTime.TryParse(Console.ReadLine(), out result))
                {
                    return result;
                }
                Console.WriteLine("Please, enter the date.");
            }
        }

        public string PromptForString()
        {
            var result = Console.ReadLine();
            return result;
        }

        public void Messager()
        {
            Console.WriteLine();
        }

        public void Messager(string massage)
        {
            Console.WriteLine(massage);
        }

        public void Messager(Type massage)
        {
            Console.WriteLine(massage);
        }

        public void Messager(string massage1, string massage2, int massage3)
        {
            Console.WriteLine("{0} {1} - {2}", massage1, massage2, massage3);
        }

        public void Messager(int massage, string massage1, string massage2, string massage4)
        {
            Console.WriteLine("{0} {1} - {2}, {3}", massage, massage1, massage2, massage4);
        }

        public void Messager(string massage1, string massage2, double massage3, int massage4)
        {
            Console.WriteLine("{0} {1} - {2}, {3}", massage1, massage2, massage3, massage4);
        }

        public void Messager(string massage1, int massage2, int massage3)
        {
            Console.WriteLine("{0} - {1} {2}", massage1, massage2, massage3);
        }
    }
}
