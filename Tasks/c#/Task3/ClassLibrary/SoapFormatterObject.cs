﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Soap;
using System.IO;

namespace ClassLibrary3
{
    public class SoapFormatterObject
    {
        private SoapFormatter _formatter = new SoapFormatter();

        public void ObjectSerialize(List<Persone> someObject)
        {
            using (FileStream fs = new FileStream("people.soap", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
            }
        }

        public void ObjectSerialize(List<Teacher> someObject)
        {
            using (FileStream fs = new FileStream("teacher.soap", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
            }
        }

        public void ObjectSerialize(List<Student> someObject)
        {
            using (FileStream fs = new FileStream("student.soap", FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, someObject);
            }
        }

        public void ObjectDeserialize(List<Persone> someObject)
        {
            using (FileStream fs = new FileStream("people.xml", FileMode.OpenOrCreate))
            {
                List<Persone> newpeople = (List<Persone>)_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }

        public void ObjectDeserialize(List<Teacher> someObject)
        {
            using (FileStream fs = new FileStream("teacher.xml", FileMode.OpenOrCreate))
            {
                List<Teacher> newpeople = (List<Teacher>)_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }

        public void ObjectDeserialize(List<Student> someObject)
        {
            using (FileStream fs = new FileStream("student.xml", FileMode.OpenOrCreate))
            {
                List<Student> newpeople = (List<Student>)_formatter.Deserialize(fs);
            }
            Console.ReadLine();
        }
    }
}
