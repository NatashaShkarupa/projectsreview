﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExtensionMethods
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> ElementSort<T, TKey>(this IEnumerable<T> source, Func<T, TKey> keySelector)
        {
            source.OrderBy(keySelector);
            foreach (var element in source)
            {
                yield return element;
            }
        }

        public static IEnumerable<T> ElementSort<T>(this IEnumerable<T> source, Comparison<T> compare)
        {
            foreach (var element in source)
            {
                source.ElementSort(compare);
                yield return element;
            }
        }

        public static IEnumerable<T> GetEveryNth<T>(this IEnumerable<T> source, int n)
        {
            if (n <= 0)
                throw new ArgumentOutOfRangeException("n");
            int index = 1;
            foreach (var element in source)
            {
                if (index % n == 0)
                    yield return element;
                index++;
            }
        }
    }
}
