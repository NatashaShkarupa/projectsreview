﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods
{
    public static class ArrayListExtentation
    {
        public static int BinarySearch(this int[] x, int searchValue, int left, int right)
        {
            if (right < left)
            {
                return -1;
            }
            int mid = (left + right) >> 1;
            if (searchValue > x[mid])
            {
                return BinarySearch(x, searchValue, mid + 1, right);
            }
            else if (searchValue < x[mid])
            {
                return BinarySearch(x, searchValue, left, mid - 1);
            }
            else
            {
                return mid;
            }
        }

        public static ArrayList Sort(this ArrayList list)
        {
            var sortedList = list.Cast<string>().OrderBy(item => int.Parse(item));
            return list;
        }
    }
}
