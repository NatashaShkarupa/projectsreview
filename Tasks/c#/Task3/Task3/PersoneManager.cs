﻿using ClassLibrary3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    [Serializable]
    public class PersoneManager
    {
        private ConsoleManager _consoleManager = new ConsoleManager();
        private BinaryFormatterObject _binaryFormatterObject = new BinaryFormatterObject();
        //private SoapFormatterObject _soapFormatterObject = new SoapFormatterObject();
        //private XmlFormatterObject _xmlFormatterObject = new XmlFormatterObject();
        public Persone _persone;
        private Student _students;
        private Teacher _teacher;
        public List<Persone> listPersone = new List<Persone>();
        public List<Student> listStudents = new List<Student>();
        public List<Teacher> listTeacher = new List<Teacher>();

        public int IdPersone { get; set; }
        public int IdStudent { get; set; }
        public int IdTeacher { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Address { get; set; }

        /// <summary>
        /// for array initialization list of people.
        /// </summary>
        public void PeopleInitialize()
        {
            _consoleManager.Messager("Insert Passport number-> ");
            IdPersone = _consoleManager.PromptForInt32();
            _consoleManager.Messager("Insert FirstName-> ");
            FirstName = _consoleManager.PromptForString();
            _consoleManager.Messager("Insert SecondName-> ");
            SecondName = _consoleManager.PromptForString();
            _consoleManager.Messager("Insert Address-> ");
            Address = _consoleManager.PromptForString();
            _persone = new Persone(IdPersone, FirstName, SecondName, Address);
            _persone.PersoneAddEvent += HandleSomeEvent;
            listPersone.Add(_persone);
       
        }

        void HandleSomeEvent(string message)
        {
            Console.WriteLine(message);
        }

        public object PeopleClone()
        {
            return _persone.Clone();
        }

        /// <summary>
        /// for array initialization list of students.
        /// </summary>
        public void StudentsInitialize()
        {
            _consoleManager.Messager("Insert NumberOfGroup-> ");
            string NumberOfGroup = _consoleManager.PromptForString();
            _consoleManager.Messager("Insert ResultOfMathematicTest-> ");
            int ResultOfMathematicTest = _consoleManager.PromptForInt32();
            _consoleManager.Messager("Insert ResultOfInformaticTest-> ");
            int ResultOfInformaticTest = _consoleManager.PromptForInt32();
            _consoleManager.Messager("Insert NumberTeacher-> ");
            int NumberTeacher = _consoleManager.PromptForInt32();
            _students = new Student(IdStudent, NumberOfGroup, ResultOfMathematicTest, ResultOfInformaticTest, NumberTeacher, IdPersone, FirstName, SecondName, Address);
            listStudents.Add(_students);
        }

        /// <summary>
        /// for array initialization list of students.
        /// </summary>
        public void TeacherInitialize()
        {
            _consoleManager.Messager("Insert subjectName-> ");
            string subjectName = _consoleManager.PromptForString();
            _consoleManager.Messager("Insert subjectBook-> ");
            string subjectBook = _consoleManager.PromptForString();
            _teacher = new Teacher(IdTeacher, subjectName, subjectBook, IdPersone, FirstName, SecondName, Address);
            listTeacher.Add(_teacher);
        }

        public void AddingByNumber(int idstudent, int idteacher)
        {
           
            var stud = listPersone.Any(x => x.IdPersone == idstudent);
            if (stud)
            {
                _consoleManager.Messager("Enter date of student:");
                StudentsInitialize();
                string studentlist = _students.ToString();
                _consoleManager.Messager(studentlist);
            }
            var teach = listPersone.Any(x => x.IdPersone == idteacher);
            if (teach)
            {
                _consoleManager.Messager("Enter date of teachers:");
                TeacherInitialize();
                string teacherlist = _teacher.ToString();
                _consoleManager.Messager(teacherlist);
            }
        }

        public void AddListOfTeacher()
        {
            for (int i = 0; i <= listTeacher.Count - 1; i++)
            {
                for (int j = 0; j <= listStudents.Count - 1; j++)
                {
                    if (listTeacher[i].IdTeacher == listStudents[j].NumberTeacher)
                    {
                        listStudents[i].AddTeacher(listTeacher[i]);
                        _students.StudentRaiseSomeEvent += HandleSomeNewEvent;
                        _students.ToString();
                    }
                }
            }
        }

        private void HandleSomeNewEvent(string msg)
        {
            Console.WriteLine(msg);
        }

        public void Start()
        {
            //Task1();
            //Task2();
            //Task4();
        }

        public void Task2()
        {
            _consoleManager.Messager("Insert path-> ");
            string path = _consoleManager.PromptForString();
            _consoleManager.Messager("Insert filetype (*.txt)-> ");
            string filetype = _consoleManager.PromptForString();
            FileWatcherClass watcher = new FileWatcherClass(path, filetype);
            watcher.Run();
        }

        public void Task1()
        {
            //task1
            _consoleManager.Messager("Insert count of persone number-> ");
            int count=0;
            int cout = _consoleManager.PromptForInt32();
            do
            {
                _consoleManager.Messager("Enter date of persone:");
                PeopleInitialize();
                _persone.Print();
                count++;
            }
            while (count != cout);

            _consoleManager.Messager("Insert Teacher passport number-> ");
            IdTeacher = _consoleManager.PromptForInt32();
            _consoleManager.Messager("Insert Student passport number-> ");
            IdStudent = _consoleManager.PromptForInt32();

            AddingByNumber(IdStudent, IdTeacher);
            AddListOfTeacher();
        }

        public void Task4()
        {
            for (int i = 0; i < 2; i++)
            {
                StudentsInitialize();
            }

            _consoleManager.Messager("Insert type of sorting (ResultOfMathematicTest-1, NumberOfGroup-2, ResultOfInformaticTest-3):-> ");
            bool ResultOfMathematicTest = false;
            bool NumberOfGroup = false;
            bool ResultOfInformaticTest = false;
            int sortType = _consoleManager.PromptForInt32();
            switch (sortType)
            {
                case 1:
                    {
                        ResultOfMathematicTest = true;
                    }
                    break;
                case 2:
                    {
                        NumberOfGroup = true;
                    }
                    break;
                case 3:
                    {
                        ResultOfInformaticTest = true;
                    }
                    break;
                default:
                    _consoleManager.Messager("Sorry, invalid number entered. Try again.");
                    break;
            }

            listStudents.Sort(new Student.StudentComparer(NumberOfGroup, ResultOfMathematicTest, ResultOfInformaticTest));

            foreach (var a in listStudents)
            {
                _consoleManager.Messager("list");
                Console.WriteLine(a.FirstName + " " + a.NumberOfGroup + " " + a.ResultOfMathematicTest + " " + a.ResultOfInformaticTest);
            }

            // clone exsample
            var newpeople = PeopleClone();
            if (newpeople.Equals(_persone))
            {
                _consoleManager.Messager("Object simular", newpeople.GetHashCode(), _persone.GetHashCode());
            }

            // Ienumerable sample
            Persons persons = new Persons();
            for (int i = 0; i < 3; i++)
            {
                PeopleInitialize();
                persons.Add(_persone);
            }
            persons.ShowList();
        }

        public void Task6()
        {
            //_binaryFormatterObject.ObjectSerialize(listPersone);
            //_soapFormatterObject.ObjectSerialize(listPersone);
            //_xmlFormatterObject.ObjectSerialize(listPersone);
        }
    }
}
