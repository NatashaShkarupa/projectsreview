﻿using ClassLibrary3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            PersoneManager personeManager = new PersoneManager();
            personeManager.Start();
            Console.ReadKey();
        }
    }
}
