﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleManager consoleManager = new ConsoleManager();
            consoleManager.Messager("Select the Task:1-3, exist-0");
            int numberOfTask = consoleManager.PromptForInt32();
            do
            {
                switch (numberOfTask)
                {
                    case 1:
                        {
                            consoleManager.Messager("Task 1");
                            PersoneManager person = new PersoneManager();
                            PersoneManager.RandomPersone();
                            //person.Start();
                        }
                        break;
                    case 2:
                        {
                            consoleManager.Messager("Task 2");
                           
                        }
                        break;
                    case 3:
                        {
                            consoleManager.Messager("Task 3.1");
                            WriteMas writeMas = new WriteMas();
                            consoleManager.Messager("Task 3.2");
                            ReadText readText = new ReadText();
                            readText.CreateMas();                        
                        }
                        break;
                    default:
                        consoleManager.Messager("Sorry, invalid number entered. Try again.");
                        break;
                }
            }
            while ((numberOfTask == 0));
            Console.ReadKey();
        }
    }
}
