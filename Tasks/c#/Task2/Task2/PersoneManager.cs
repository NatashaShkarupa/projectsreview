﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    public class PersoneManager
    {
        private ConsoleManager consoleManager = new ConsoleManager();
        private Persone persone;
        private Students students;
        private Teacher teacher;
        public List<Persone> listPersone = new List<Persone>();
        public List<Students> listStudents = new List<Students>();
        public List<Teacher> listTeacher = new List<Teacher>();

        public int idPersone { get; set; }
        public int idStudent { get; set; }
        public int idTeacher { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Address { get; set; }

        /// <summary>
        /// for array initialization list of people.
        /// </summary>
        public void PeopleInitialize()
        {
            consoleManager.Messager("Insert Passport number-> ");
            idPersone = consoleManager.PromptForInt32();
            consoleManager.Messager("Insert FirstName-> ");
            FirstName = consoleManager.PromptForString();
            consoleManager.Messager("Insert SecondName-> ");
            SecondName = consoleManager.PromptForString();
            consoleManager.Messager("Insert Address-> ");
            Address = consoleManager.PromptForString();
            persone = new Persone(idPersone, FirstName, SecondName, Address);
            listPersone.Add(persone);
        }

        public object PeopleClone()
        {
            return persone.Clone();          
        }

        /// <summary>
        /// for array initialization list of students.
        /// </summary>
        public void StudentsInitialize()
        {
            consoleManager.Messager("Insert NumberOfGroup-> ");
            string NumberOfGroup = consoleManager.PromptForString();
            consoleManager.Messager("Insert ResultOfMathematicTest-> ");
            int ResultOfMathematicTest = consoleManager.PromptForInt32();
            consoleManager.Messager("Insert ResultOfInformaticTest-> ");
            int ResultOfInformaticTest = consoleManager.PromptForInt32();
            consoleManager.Messager("Insert NumberTeacher-> ");
            int NumberTeacher = consoleManager.PromptForInt32();
            students = new Students(idStudent, NumberOfGroup, ResultOfMathematicTest, ResultOfInformaticTest, NumberTeacher, idPersone, FirstName, SecondName, Address);
            listStudents.Add(students);
        }

        /// <summary>
        /// for array initialization list of students.
        /// </summary>
        public void TeacherInitialize()
        {
            consoleManager.Messager("Insert NumberOfGroup-> ");
            string subjectName = consoleManager.PromptForString();
            consoleManager.Messager("Insert ResultOfMathematicTest-> ");
            string subjectBook = consoleManager.PromptForString();
            teacher = new Teacher(idTeacher, subjectName, subjectBook, idPersone, FirstName, SecondName, Address);
            listTeacher.Add(teacher);
        }

        public void GetInheritance()
        {
            Type ourtype = typeof(Persone);
            IEnumerable<Type> list = Assembly.GetAssembly(ourtype).GetTypes().Where(type => type.IsSubclassOf(ourtype));

            foreach (Type itm in list)
            {
                consoleManager.Messager(itm);
                var derived = itm;
                do
                {
                    derived = derived.BaseType;
                    if (derived != null)
                        consoleManager.Messager(derived.FullName);
                } while (derived != null);
                consoleManager.Messager();
            }
        }

        public void AddingByNumber(int idstudent, int idteacher)
        {
            if (listStudents[idstudent].Equals(listPersone[idPersone]))
            {
                consoleManager.Messager("Enter date of student:");
                StudentsInitialize();
                string studentlist = students.ToString();
                consoleManager.Messager(studentlist);
            }
            else
            {
                if (listTeacher[idteacher].Equals(listPersone[idPersone]))
                {
                    consoleManager.Messager("Enter date of teachers:");
                    TeacherInitialize();
                    string teacherlist = teacher.ToString();
                    consoleManager.Messager(teacherlist);
                }
            }
        }

        /// <summary>
        /// get random element of persone collection.
        /// </summary>
        public static void RandomPersone()
        {
            ConsoleManager consoleManager = new ConsoleManager();
            Persone[] mas = new Persone[3];
            for (int counter = 0; counter < 3; counter++)
            {
                consoleManager.Messager("Insert Passport number-> ");
                int idPersone = consoleManager.PromptForInt32();
                consoleManager.Messager("Insert FirstName-> ");
                string FirstName = consoleManager.PromptForString();
                consoleManager.Messager("Insert SecondName-> ");
                string SecondName = consoleManager.PromptForString();
                consoleManager.Messager("Insert Address-> ");
                string Address = consoleManager.PromptForString();
                mas[counter] = new Persone(idPersone, FirstName, SecondName, Address);
            }
            int randomElement = new Random().Next(0, mas.Length);
            consoleManager.Messager(mas[randomElement].idPersone, mas[randomElement].firstName, mas[randomElement].secondName, mas[randomElement].address);
        }

        public void AddListOfTeacher()
        {
            for (int i = 0; i <= listTeacher.Count-1; i++)
            {
                for (int j = 0; j <= listStudents.Count-1; j++)
                {
                    if (listTeacher[j].idTeacher == listStudents[i].numberTeacher)
                    {
                        TeacherInitialize();
                        listStudents[i].AddTeacher(listTeacher[j]);
                    }
                }
            }
        }

        /// <summary>
        /// for start works with collections.
        /// </summary>
        public void Start()
        {
            consoleManager.Messager("Enter date of persone:");
            PeopleInitialize();

            consoleManager.Messager("Insert Teacher passport number-> ");
            idTeacher = consoleManager.PromptForInt32();
            consoleManager.Messager("Insert Student passport number-> ");
            idStudent = consoleManager.PromptForInt32();

            AddingByNumber(idStudent, idTeacher);

            AddListOfTeacher();

            // clone exsample
            var newpeople = PeopleClone();
            if (newpeople.Equals(persone))
            {
                consoleManager.Messager("Object simular", newpeople.GetHashCode(), persone.GetHashCode());
            }

            // class(Persone) inheritance
            consoleManager.Messager("Persone inheritance");
            GetInheritance();
        }
    }
    }
