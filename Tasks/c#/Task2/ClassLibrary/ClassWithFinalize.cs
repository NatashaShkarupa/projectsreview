﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class ClassWithFinalize :IDisposable
    {
        private bool disposed = false;

        public ConsoleManager consoleManager = new ConsoleManager();
        public FileStream fs = new FileStream(@"C:\Users\1\Desktop\Univesity\file2.txt", FileMode.OpenOrCreate);

        /// <summary>
        /// Method designed for explicit release of resources.
        /// </summary>
        public void Dispose() {
            Console.WriteLine("\n[{0}].fs.Dispose()");
            Dispose(true);
            // Use SupressFinalize in case a subclass of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(Boolean disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!disposed)
            {
                if (disposing)
                {
                    if (fs != null)
                        fs.Dispose();
                    consoleManager.Messager("Object disposed.");
                }

                // Indicate that the instance has been disposed.
                fs = null;
                disposed = true;
            }
        }

        /// <summary>
        /// Destructor syntax for finalization code.
        /// </summary>
        ~ClassWithFinalize()
        {
            Dispose(false);
            consoleManager.Messager("Object fs disposed.");
        }
    }

    public class ReadText : ClassWithFinalize
    {
        /// <summary>
        /// Method designed for reading the file numbers of the double-precision calculating their sum.
        /// </summary>
        public void CreateMas()
        {
            var fileMas = new StreamReader(fs);
            
                var line = fileMas.ReadLine();
                if (line == null)
                {
                    throw new System.ArgumentException("expect line in input file");
                }
                Regex regex = new Regex(@"[0-9]{1,3}([.,][0-9]{1,3})?");
                MatchCollection matches = regex.Matches(line);
                double sum = 0;
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        consoleManager.Messager(match.Value);
                        sum += Convert.ToDouble(match.Value);
                    }
                    consoleManager.Messager("Sum= " + sum);
                }
            fileMas.Dispose();          
        }
    }
}
