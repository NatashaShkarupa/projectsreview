﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    /// <summary>
    /// Class Students inheritor to the Person
    /// </summary>
    public class Students : Persone
    {
        public string numberOfGroup { get; set; }
        public int resultOfMathematicTest { get; set; }
        public int resultOfInformaticTest { get; set; }
        public int numberTeacher { get; set; }
        public int idStudents { get; set; }

        public Students(int idStudents, string NumberOfGroup, int ResultOfMathematicTest, int ResultOfInformaticTest, int NumberTeacher,
            int idPersone, string FirstName, string SecondName, string Address)
            : base(idPersone, FirstName, SecondName, Address)
        {
            this.idStudents = idStudents;
            this.numberOfGroup = numberOfGroup;
            this.numberTeacher = NumberTeacher;
            this.resultOfInformaticTest = ResultOfInformaticTest;
            this.resultOfMathematicTest = ResultOfMathematicTest;
        }

        public override string ToString()
        {
            return "Students: " + idStudents + " " + firstName + " " + secondName + " - " + numberOfGroup
                + " " + resultOfMathematicTest + " " + resultOfInformaticTest + " " + numberTeacher;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            Students objAsPart = obj as Students;
            if (objAsPart == null)
            {
                return false;
            }
            else return Equals(objAsPart);
        }

        public override int GetHashCode()
        {
            return idStudents;
        }

        public bool Equals(Students other)
        {
            if (other == null) return false;
            return (this.idStudents.Equals(other.idStudents));
        }

        private List<Teacher> studentList = new List<Teacher>();

        public void AddTeacher(Teacher teacher)
        {
            studentList.Add(teacher);
        }

        public bool RemoveTeacher(Teacher teacher)
        {
            if (studentList.Exists(x => x == teacher))
            {
                return studentList.Remove(teacher);
            }
            else
            {
                return false;
            }
        }
    }
}
