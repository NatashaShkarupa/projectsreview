﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    /// <summary>
    /// Class BaggageRoom inheritor to the Person
    /// </summary>
    public class Teacher : Persone
    {
        public string subjectName { get; set; }
        public string subjectBook { get; set; }
        public int idTeacher { get; set; }

        public Teacher(int idTeacher, string SubjectName, string SubjectBook, int idPersone, string FirstName, string SecondName,
            string Address)
            : base(idPersone, FirstName, SecondName, Address)
        {
            this.idPersone = idTeacher;
            this.subjectName = SubjectName;
            this.subjectBook = SubjectBook;
        }

        public override string ToString()
        {
            return "Students: " + idTeacher + " " + firstName + " " + secondName + " - " + subjectName + " " + subjectBook;
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            Teacher objAsPart = obj as Teacher;
            if (objAsPart == null)
            {
                return false;
            }
            else return Equals(objAsPart);
        }

        public override int GetHashCode()
        {
            return idTeacher;
        }

        public bool Equals(Teacher other)
        {
            if (other == null) return false;
            return (this.idTeacher.Equals(other.idTeacher));
        }

        private List<Students> studentList = new List<Students>();

        public void AddStudent(Students student)
        {
            studentList.Add(student);
        }

        public bool RemoveStudent(Students student)
        {
            //var test=list_student.Select(x => x == student);
            if (studentList.Exists(x => x == student))
            {
                return studentList.Remove(student);
            }
            else
            {
                return false;
            }
        }
    }
}
