﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class CreateMas
    {
        public ConsoleManager consoleManager = new ConsoleManager();
        public int[] mas;

        public CreateMas()
        {
            consoleManager.Messager("Enter the size of the matrix.");
            int row = consoleManager.PromptForInt32();
            mas = new int[row];
            Random rand = new Random();
            for (int i = 0; i < row; i++)
                mas[i] = rand.Next(-10, 10);
        }
    }

    public class WriteMas:CreateMas
    {
        private FileStream fs = new FileStream(@"C:\Users\1\Desktop\Univesity\file.txt", FileMode.OpenOrCreate);

        public void Read()
        {
            using (StreamWriter sr = new StreamWriter(fs))
            {
                foreach (var t in mas)
                {
                    consoleManager.Messager(t + " ");
                    sr.Write(t + " ");
                }
            }
        }

        /// <summary>
        /// Method designed for writing in file squared of element.
        /// </summary>
        public void WriteStep()
        {
            using (StreamWriter sr = new StreamWriter(@"C:\Users\1\Desktop\Univesity\file.txt"))
            {
                foreach (var t in mas)
                {
                    var lineSquared = t * t;
                    consoleManager.Messager(lineSquared + " ");
                    sr.Write(lineSquared + " ");
                }
            }
        }

        public void Start()
        {
            CreateMas createMas = new CreateMas();
            Read();
            WriteStep();
        }
    }
}
