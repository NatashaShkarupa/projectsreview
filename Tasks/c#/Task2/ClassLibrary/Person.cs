﻿using System;


namespace ClassLibrary
{
    /// <summary>
    /// Base class.
    /// </summary>
    public class Persone:ICloneable
    {
        public int idPersone { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string address { get; set; }
        public int numberOfSchool { get; set; }
        ConsoleManager consoleManager = new ConsoleManager();

        public Persone(int idPersone, string FirstName, string SecondName, string Address)
        {
            this.idPersone = idPersone;
            this.firstName = FirstName;
            this.secondName = SecondName;
            this.address = Address;
        }

        virtual public void Print()
        {
            consoleManager.Messager(idPersone, firstName, secondName, address);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            Persone objAsPart = obj as Persone;
            if (objAsPart == null)
            {
                return false;
            }
            else return Equals(objAsPart);
        }

        public override int GetHashCode()
        {
            return idPersone;
        }

        public bool Equals(Persone other)
        {
            if (other == null) return false;
            return (this.idPersone.Equals(other.idPersone));
        }

        /// <summary>
        ///  Shallow copys.
        /// </summary>
        //public object Clone()
        //{
        //    return this.MemberwiseClone();
        //}

        /// <summary>
        ///  Deep copy.
        /// </summary>
        public object Clone()
        {
            return new Persone (idPersone=this.idPersone, firstName = this.firstName, secondName = this.secondName, address =this.address);
        }
    }
}
