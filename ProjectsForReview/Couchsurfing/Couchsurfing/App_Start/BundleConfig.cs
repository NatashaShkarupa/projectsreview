﻿using System.Web;
using System.Web.Optimization;

namespace Couchsurfing
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/bootstrap/css").Include(
                      "~/Content/bootstrap/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/ChatStyle.css",
                      "~/Content/HomeStyle.css",
                      "~/Content/ImageZoomStyle.css",
                      "~/Content/ModalStyle.css",
                      "~/Content/ProfileStyle.css"
                      ));

            bundles.Add(new ScriptBundle("~/Scripts").Include(
                       "~/Scripts/MySlider.js",
                       "~/Scripts/tabs.js"
          //"~/Content/HomeStyle.css",
          //"~/Content/ImageZoomStyle.css",
          //"~/Content/ModalStyle.css",
          //"~/Content/ProfileStyle.css"
          ));

        }
    }
}
