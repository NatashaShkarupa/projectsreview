﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Couchsurfing.BL.ViewModels;
using Couchsurfing.BL.Manager;
using Microsoft.AspNet.Identity;

namespace Couchsurfing.Hubs
{
    public class ChatHub : Hub
    {
        //static ChatModel chatModel;
        ChatManager chatManager = new ChatManager();
        ChatMessage chatmessager = new ChatMessage();
        ChatUser chatUser = new ChatUser();

        static ChatModel chatModel=new ChatModel();

        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }

        public void SendMessage(String login, String msg, String roomName)
        {
            Clients.Group(roomName).addMessage(login, msg);
        }

        public void SendPrivateMessage(string toUserId, string message)
        {
            string fromUserId = Context.ConnectionId;

            var toUser = chatModel.Users.FirstOrDefault(x => x.ConnectionId == toUserId);
            var fromUser = chatModel.Users.FirstOrDefault(x => x.ConnectionId == fromUserId);

            if (toUser != null && fromUser != null)
            {
                // send to
                Clients.Client(toUserId).sendPrivateMessage(fromUserId, fromUser.FirstNameUser, message);

                // send to caller user
                Clients.Caller.sendPrivateMessage(toUserId, fromUser.FirstNameUser, message);
            }
        }

        // Подключение нового пользователя
        public void Connect()
        {
            string idUser = Context.User.Identity.GetUserId();
            string userName=chatManager.GetName(idUser);
            var avatar = chatManager.GetAvatarByIdRegistration(idUser);
            var id = Context.ConnectionId;


            if (!chatModel.Users.Any(x => x.ConnectionId == id))
            {
                chatModel.Users.Add(new ChatUser { ConnectionId = id, FirstNameUser = userName, Avatar=avatar });

                // Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(id, userName, avatar, chatModel.Users);

                // Посылаем сообщение всем пользователям, кроме текущего
                Clients.AllExcept(id).onNewUserConnected(id, userName, avatar);
            }
        }

        // Отключение пользователя
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = chatModel.Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                chatModel.Users.Remove(item);
                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.FirstNameUser);
            }

            return base.OnDisconnected(stopCalled);
        }

        //public Task LeaveChat(String roomName)
        //{
        //    return Groups.Remove(Context.ConnectionId, roomName);
        //}
    }
}