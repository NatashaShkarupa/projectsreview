﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class TripRepository : BaseRepository, IRepository<Trip>
    {

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }
        public int GetUserIdByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
                return context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.Id).SingleOrDefault();
            }
        }
        public IEnumerable<ToDoList> GetToDoList(int id)
        {
            using (var context = GetContext())
            {
                
                var city = context.ToDoList.Where(x => x.Trip_id == id);
                
                return context.ToDoList.ToList();
            }
        }
        public IEnumerable<Trip> GetList(string id)
        {
            using (var context = GetContext())
            {
                int temp = GetUserIdByIdRegistration(id);
                var city = context.Trip.Where(x => x.User_profile_id == temp);
                /*if (city == null)
                {
                    context.City.Add(city);
                    context.SaveChanges();
                }*/
                return context.Trip.ToList();
            }
        }

        public void Add(Trip newobject)
        {
            using (var context = GetContext())
            {
                context.Trip.Add(newobject);
                context.SaveChanges();
            }
        }
        public void AddToDoList(int id)
        {
            using (var context = GetContext())
            {
                var newobject = new ToDoList() { Status = 0, Trip_id = id, ToDoList_text = "Default text" };
                context.ToDoList.Add(newobject);
                context.SaveChanges();
            }
        }
        public void DeleteList(int id, int tripid)
        {
            using (var context = GetContext())
            {
                var item = context.ToDoList.Where(x => x.Trip_id == tripid && x.Id == id).First();
                context.ToDoList.Remove(item);
                context.SaveChanges();
            }
        }
        public void AddToDoList(ToDoList obj)
        {
            using (var context = GetContext())
            {
                context.ToDoList.Add(obj);
                context.SaveChanges();
            }
        }
        public string FindByIdLivingUser(string idRegistration)
        {
            using (var context = GetContext())
            {
                var idLiving = context.UserProfile.Where(x => x.UserRegistration_id == idRegistration).Select(x => x.Living_place_id).FirstOrDefault();
                var adress = context.LivingPlace.Where(x => x.Id == idLiving).Select(x => x.UAddress_id).FirstOrDefault();

                var idCountry = context.LivingPlace.Where(x => x.Id == idLiving).Select(x => x.Country_id).FirstOrDefault();
                var idCity = context.LivingPlace.Where(x => x.Id == idLiving).Select(x => x.City_id).FirstOrDefault();
                var country = context.Country.Where(x => x.Id == idCountry).Select(x => x.Label).FirstOrDefault();
                var city = context.City.Where(x => x.Id == idCity).Select(x => x.Label).FirstOrDefault();

                return city;
            }
        }

        public IEnumerable<Trip> GetList()
        {
            throw new NotImplementedException();
        }

        public void EditRow(Trip item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}