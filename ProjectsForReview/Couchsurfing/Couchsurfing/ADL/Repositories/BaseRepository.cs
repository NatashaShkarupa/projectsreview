﻿using Couchsurfing.ADL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Couchsurfing.ADL.Repositories
{
    public abstract class BaseRepository
    {
        protected CouchsurfingEntities GetContext()
        {
            return new CouchsurfingEntities();
        }
    }
}
