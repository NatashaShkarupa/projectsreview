﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class LivingPlaceRepository : BaseRepository, IRepository<LivingPlace>
    {
        CityRepository _city = new CityRepository();
        CountryRepository _country = new CountryRepository();
        public IEnumerable<LivingPlace> GetList()
        {
            using (var context = GetContext())
            {
                return context.LivingPlace.ToList();
            }
        }

        public void AddCategory(LivingPlace newobject)
        {
            using (var context = GetContext())
            {
                context.LivingPlace.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Edit(LivingPlace newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.LivingPlace.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.LivingPlace.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public int FindIdByCountryCity(int country, int city)
        {
            using (var context = GetContext())
            {
                var id = context.LivingPlace.Where(x => (x.Country_id == country) && (x.City_id == city)).Select(x => x.Id).SingleOrDefault();
                {
                    return id;
                }
            }
        }

        public int FindIdByCountryCityString(string address, string country, string city)
        {
            using (var context = GetContext())
            {
                int cityId = _city.FindByName(city);
                int countryId = _country.FindByName(country);
                var id = context.LivingPlace.Where(x => (x.Country_id == countryId) && (x.City_id == cityId) && (x.UAddress_id==address)).Select(x => x.Id).SingleOrDefault();
                {
                    return id;
                }
            }
        }

        public void Add(LivingPlace item)
        {
            throw new NotImplementedException();
        }

        public void EditRow(LivingPlace item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}