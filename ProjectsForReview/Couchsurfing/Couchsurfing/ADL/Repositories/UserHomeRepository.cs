﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class UserHomeRepository : BaseRepository, IRepository<UserHome>
    {
        public void Edit(UserHome newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<SleepingArrangement> GetListSleepingArrangement()
        {
            using (var context = GetContext())
            {
                return context.SleepingArrangement.ToList();
            }
        }

        public IEnumerable<MiscellaneousConnection> GetListMiscellaneous()
        {
            using (var context = GetContext())
            {
                return context.MiscellaneousConnection.ToList();
            }
        }

        public IEnumerable<UserHome> GetList()
        {
            using (var context = GetContext())
            {
                return context.UserHome.ToList();
            }
        }

        public IEnumerable<UserHome> GetListById(string id)
        {
            using (var context = GetContext())
            {
                var userId= context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.Id).SingleOrDefault();
                IEnumerable<UserHome> result =context.UserHome.Where(x => x.User_profile_id == userId).ToList();
                return result;
            }
        }

        public string GetArangment(string id)
        {
            using (var context = GetContext())
            {
                int userId = context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.Id).SingleOrDefault();
                int? slipId = context.UserHome.Where(x => x.User_profile_id == userId).Select(x=>x.Sleeping_arrangement_id).SingleOrDefault();
                string text = context.SleepingArrangement.Where(x => x.Id == slipId).Select(x => x.Label).SingleOrDefault();
                return text;
            }
        }

        public void Add(UserHome newobject)
        {
            using (var context = GetContext())
            {
                context.UserHome.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserHome.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.UserHome.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public int GetId(string id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserProfile.FirstOrDefault(x => x.UserRegistration_id == id);
                return newobject.Id;
            }
        }

        public UserHome GetById(int id)
        {
            using (var context = GetContext())
            {
                int quaryid = context.UserHome.Where(x => x.User_profile_id == id).Select(x=>x.Id).SingleOrDefault();
                var quary = context.UserHome.Find(quaryid);
                return quary;
            }
        }

        public void AddEditHome(UserHome home, int id)
        {
            using (var context = GetContext())
            {
                var quary = GetById(id);
                if (quary == null) Add(home);
                else {
                    home.Id = quary.Id;
                    Edit(home); }
            }
        }

        public void EditRow(UserHome item)
        {
            throw new NotImplementedException();
        }
    }
}