﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;
using System.Data.Entity.Validation;

namespace Couchsurfing.ADL.Repositories
{ 

    public class UserProfileRepository : BaseRepository, IRepository<UserProfile>
    {
        CityRepository _city = new CityRepository();
        CountryRepository _country = new CountryRepository();

        public IEnumerable<UserProfile> GetList()
        {
            using (var context = GetContext())
            {

                return context.UserProfile.ToList();
            }
        }

        public void Edit(UserProfile newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Edit(UserProfile newobject, string id)
        {
            using (var context = GetContext())
            {
                //UserProfile userObj = context.UserProfile.Where(x => x.UserRegistration_id == id).SingleOrDefault();
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public IEnumerable<UserProfile> GetListByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
                return context.UserProfile.Where(x => x.UserRegistration_id == id).ToList();
            }
        }

        public byte[] GetAvatarByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
               var a= context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x=>x.Avatar).FirstOrDefault();
                return a;
            }
        }

        public IEnumerable<Photo> GetListPhotosByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
                var idUser = GetId(id);
                return context.Photo.Where(x => x.User_Id == idUser).ToList();
            }
        }

        public int GetUserIdByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
                return context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.Id).SingleOrDefault();
            }
        }

        public string GetUserNameByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
                var firstname= context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.FirstName).SingleOrDefault();
                var secondname = context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.SecondName).SingleOrDefault();
                return firstname+" " + secondname;
            }
        }

        public void AddCategory(UserProfile newobject)
        {
            using (var context = GetContext())
            {
                context.UserProfile.Add(newobject);
                context.SaveChanges();
            }
        }

        public void AddPhotos(Photo newobject)
        {
            using (var context = GetContext())
            {
                context.Photo.Add(newobject);
                context.SaveChanges();
            }
        }

        public UserProfile GetById(int id)
        {
            using (var context = GetContext())
            {
                return context.UserProfile.Find(id);
            }
        }

        public Photo GetByIdPhotos(int id)
        {
            using (var context = GetContext())
            {
                return context.Photo.Find(id);
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserProfile.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.UserProfile.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public int GetId(string id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserProfile.FirstOrDefault(x => x.UserRegistration_id == id);
                return newobject.Id;
            }
        }

        public string GetName(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserProfile.FirstOrDefault(x => x.Id == id);
                return newobject.FirstName + " " + newobject.SecondName;
            }
        }

        public string FindByIdLivingUser(string idRegistration)
        {
            using (var context = GetContext())
            {
                var idLiving = context.UserProfile.Where(x => x.UserRegistration_id == idRegistration).Select(x => x.Living_place_id).FirstOrDefault();
                var adress = context.LivingPlace.Where(x => x.Id == idLiving).Select(x => x.UAddress_id).FirstOrDefault();

                var idCountry = context.LivingPlace.Where(x => x.Id == idLiving).Select(x => x.Country_id).FirstOrDefault();
                var idCity = context.LivingPlace.Where(x => x.Id == idLiving).Select(x => x.City_id).FirstOrDefault();
                var country = context.Country.Where(x => x.Id == idCountry).Select(x => x.Label).FirstOrDefault();
                var city = context.City.Where(x => x.Id == idCity).Select(x => x.Label).FirstOrDefault();
                if (adress != null)
                {
                    return adress + "," + city + "," + country;
                }
                return city + "," + country;
            }
        }

        public void UpdateUserLiving(string address,string city, string country, string idRegistration)
        {
            using (var context = GetContext())
            {
                int cityId=_city.FindByName(city);
                int countryId = _country.FindByName(country);
                int idLiving = context.UserProfile.Where(x => x.UserRegistration_id == idRegistration).Select(x => x.Living_place_id).FirstOrDefault();
                LivingPlace newobject = context.LivingPlace.Where(x => x.Id == idLiving).FirstOrDefault();

                newobject.Country_id = countryId;
                newobject.City_id = cityId;
                newobject.UAddress_id = address;
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Add(UserProfile item)
        {
            throw new NotImplementedException();
        }

        public void EditRow(UserProfile item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}