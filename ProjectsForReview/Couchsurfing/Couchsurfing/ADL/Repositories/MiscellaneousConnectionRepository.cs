﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class MiscellaneousConnectionRepository : BaseRepository, IRepository<MiscellaneousConnection>
    {
        public void EditRow(MiscellaneousConnection newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<MiscellaneousConnection> GetList()
        {
            using (var context = GetContext())
            {
                return context.MiscellaneousConnection.ToList();
            }
        }

        public void AddCategory(MiscellaneousConnection newobject)
        {
            using (var context = GetContext())
            {
                context.MiscellaneousConnection.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.MiscellaneousConnection.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.MiscellaneousConnection.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Add(MiscellaneousConnection item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}