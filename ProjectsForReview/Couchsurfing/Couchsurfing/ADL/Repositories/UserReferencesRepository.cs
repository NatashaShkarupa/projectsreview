﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class UserReferencesRepository : BaseRepository, IRepository<UserReferences>
    {
        public void EditRow(UserReferences newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<UserReferences> GetList()
        {
            using (var context = GetContext())
            {
                return context.UserReferences.ToList();
            }
        }

        public void AddCategory(UserReferences newobject)
        {
            using (var context = GetContext())
            {
                context.UserReferences.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserReferences.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.UserReferences.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Add(UserReferences item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}