﻿using Couchsurfing.ADL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Couchsurfing.ADL.Repositories
{
    public class SearchRepository:BaseRepository, IRepository<UserProfile>
    {
        CityRepository _city = new CityRepository();
        CountryRepository _country = new CountryRepository();

        public IEnumerable<UserProfile> SearchByName(string searchString)
        {
            using (var context = GetContext())
            {
                var quary = context.UserProfile.Where(s => s.FirstName.Contains(searchString)
                                || s.SecondName.Contains(searchString)).ToList();
                 return quary;
            }
        }

        public IEnumerable<UserProfile> SearchByLiving(string country, string city)
        {
            using (var context = GetContext())
            {
                int cityId = _city.FindByName(city);
                int countryId = _country.FindByName(country);
                var res = context.LivingPlace.Where(x => x.Country_id == countryId).ToList();
                int idLiving = res.Where(x => x.City_id==cityId).Select(x => x.Id).FirstOrDefault();
                var quary = context.UserProfile.Where(s => s.Living_place_id== idLiving).ToList();
                return quary;
            }
        }
        
        public IEnumerable<UserProfile> SearchByAll(int arangment, string status, string sex, int max_number, 
            string language, int fromyear, int toage)
        {
            using (var context = GetContext())
            {
                DateTime now = DateTime.Today;
                var quaryHomeId = context.UserHome.Where(x => x.Sleeping_arrangement_id == arangment).Select(x=>x.User_profile_id).FirstOrDefault();
                var quaryUser = context.UserProfile.Where(x => x.Sex == sex && x.User_status == status && x.Languages == language
                && x.Id == quaryHomeId && ((now.Year - x.Birthday.Value.Year) > fromyear)
                                && ((now.Year - x.Birthday.Value.Year) < toage)
                ).ToList();

                //var quary = (from u in context.UserProfile
                //             join h in context.UserHome
                //             on u.Id equals h.User_profile_id
                //             join s in context.SleepingArrangement
                //             on h.Sleeping_arrangement_id equals s.Id
                //             join m in context.MiscellaneousConnection
                //             on h.Id equals m.User_home_id
                //             where ((u.User_status == status) && (u.Sex == sex) && (s.Id == arangment) && (u.Languages==language) 
                //            )
                //             select u
                //).ToList();
                return quaryUser;
            }
        }

        public IEnumerable<UserProfile> SearchByAllWithName(string searchString, int arangment, string status, string sex, int max_number, 
            string language, int fromyear, int toage)
        {
            using (var context = GetContext())
            {
                DateTime now = DateTime.Today;
                var quaryHomeId = context.UserHome.Where(x => x.Sleeping_arrangement_id == arangment).Select(x => x.User_profile_id).FirstOrDefault();
                var quaryUser = context.UserProfile.Where(x => x.Sex == sex && x.User_status == status && x.Languages == language
                && x.Id == quaryHomeId && (x.FirstName.Contains(searchString)|| (x.SecondName.Contains(searchString)) )
                && ((now.Year - x.Birthday.Value.Year) > fromyear) && ((now.Year - x.Birthday.Value.Year) < toage)
                ).ToList();
                //var quary = (from u in context.UserProfile
                //             join h in context.UserHome
                //             on u.Id equals h.User_profile_id
                //             join s in context.SleepingArrangement
                //             on h.Sleeping_arrangement_id equals s.Id
                //             join m in context.MiscellaneousConnection
                //             on h.Id equals m.User_home_id
                //             where ( (u.FirstName==searchString) && (u.SecondName==searchString) && (u.User_status == status) && (u.Sex == sex) 
                //                && (s.Id == arangment) && (u.Languages == language) && ((now.Year - u.Birthday.Value.Year) > fromyear) 
                //                && ((now.Year - u.Birthday.Value.Year) < toage))
                //             select u
                //).ToList();
                return quaryUser;
            }
        }

        public IEnumerable<UserProfile> GetList()
        {
            throw new NotImplementedException();
        }

        public void Add(UserProfile item)
        {
            throw new NotImplementedException();
        }

        public void EditRow(UserProfile item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}