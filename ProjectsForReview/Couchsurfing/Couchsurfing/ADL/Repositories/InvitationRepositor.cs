﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class InvitationRepositor : BaseRepository, IRepository<Invitation>
    {
        public void EditRow(Invitation newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<Invitation> GetList()
        {
            using (var context = GetContext())
            {
                return context.Invitation.ToList();
            }
        }

        public void AddCategory(Invitation newobject)
        {
            using (var context = GetContext())
            {
                context.Invitation.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.Invitation.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.Invitation.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Add(Invitation item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}