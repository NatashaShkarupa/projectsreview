﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class ReferenceTypeRepository : BaseRepository, IRepository<ReferenceType>
    {
        public void Add(ReferenceType item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void EditRow(ReferenceType item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ReferenceType> GetList()
        {
            using (var context = GetContext())
            {
                return context.ReferenceType.ToList();
            }
        }
    }
}