﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class CityRepository : BaseRepository, IRepository<City>
    {
        public void EditRow(City newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<City> GetList()
        {
            using (var context = GetContext())
            {
                return context.City.ToList();
            }
        }

        public void AddCategory(City newobject)
        {
            using (var context = GetContext())
            {
                var city = context.City.FirstOrDefault(x => x.Label == newobject.Label);
                if (city ==null)
                {
                    context.City.Add(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Edit(City newobject)
        {
            using (var context = GetContext())
            {
                var city = context.City.FirstOrDefault(x => x.Label == newobject.Label);
                if (city == null)
                {
                    context.Entry(newobject).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        public int FindByName(string newobject)
        {
            using (var context = GetContext())
            {
                var id = context.City.Where(x=> x.Label== newobject).FirstOrDefault();
                    if (id == null)
                    {
                    City newcity = new City();
                    newcity.Label = newobject;
                    AddCategory(newcity);
                    Save();
                }
                var idSelect = context.City.Where(x => x.Label == newobject).Select(x => x.Id).FirstOrDefault();
                return idSelect;
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void Add(City item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}