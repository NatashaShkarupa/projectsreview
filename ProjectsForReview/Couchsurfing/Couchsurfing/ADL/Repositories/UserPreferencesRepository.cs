﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class UserPreferencesRepository: BaseRepository, IRepository<UserPreferences>
    {
        public void EditRow(UserPreferences newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<UserPreferences> GetList()
        {
            using (var context = GetContext())
            {
                return context.UserPreferences.ToList();
            }
        }

        public void AddCategory(UserPreferences newobject)
        {
            using (var context = GetContext())
            {
                context.UserPreferences.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.UserPreferences.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.UserPreferences.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Add(UserPreferences item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
