﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class PhotoRepository : BaseRepository, IRepository<Photo>
    {
        public void EditRow(Photo newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<Photo> GetList()
        {
            using (var context = GetContext())
            {
                return context.Photo.ToList();
            }
        }

        public void AddCategory(Photo newobject)
        {
            using (var context = GetContext())
            {
                context.Photo.Add(newobject);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void DeleteRow(int id)
        {
            using (var context = GetContext())
            {
                var newobject = context.Photo.FirstOrDefault(x => x.Id == id);
                if (newobject != null)
                {
                    context.Photo.Remove(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Add(Photo item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}