﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Couchsurfing.ADL.Model;

namespace Couchsurfing.ADL.Repositories
{
    public class CountryRepository : BaseRepository, IRepository<Country>
    {
        public void EditRow(Country newobject)
        {
            using (var context = GetContext())
            {
                context.Entry(newobject).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public IEnumerable<Country> GetList()
        {
            using (var context = GetContext())
            {
                return context.Country.ToList();
            }
        }

        public void AddCategory(Country newobject)
        {
            using (var context = GetContext())
            {
                var country = context.Country.FirstOrDefault(x=>x.Label==newobject.Label);
                if (country ==null)
                {
                    context.Country.Add(newobject);
                    context.SaveChanges();
                }
            }
        }

        public void Edit(Country newobject)
        {
            using (var context = GetContext())
            {
                var country = context.Country.FirstOrDefault(x => x.Label == newobject.Label);
                if (country == null)
                {
                    context.Entry(newobject).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
        }

        public int FindByName(string newobject)
        {
            using (var context = GetContext())
            {
                var id = context.Country.Where(x => x.Label == newobject).FirstOrDefault();
                if (id == null)
                {
                    Country newcountry = new Country();
                    newcountry.Label = newobject;
                    AddCategory(newcountry);
                    Save();
                }
                var idSelect = context.Country.Where(x => x.Label == newobject).Select(x => x.Id).FirstOrDefault();
                return idSelect;
            }
        }


        public string FindById(int idUser)
        {
            using (var context = GetContext())
            {
                var id = context.Country.Where(x => x.Id == idUser).Select(x => x.Label).FirstOrDefault();
                {
                    return id;
                }
            }
        }

        public void Save()
        {
            using (var context = GetContext())
            {
                context.SaveChanges();
            }
        }

        public void Add(Country item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}