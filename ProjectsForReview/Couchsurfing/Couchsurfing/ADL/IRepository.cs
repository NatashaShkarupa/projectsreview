﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Couchsurfing.ADL
{
    interface IRepository<T> where T : class
    {
        IEnumerable<T> GetList();
        void Add(T item);
        void EditRow(T item);
        void Delete(int id);
    }
}
