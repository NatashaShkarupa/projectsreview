//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Couchsurfing.ADL.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ToDoList
    {
        public int Id { get; set; }
        public int Trip_id { get; set; }
        public string ToDoList_text { get; set; }
        public int Status { get; set; }
        public int ToDoList_order { get; set; }
    
        public virtual Trip Trip { get; set; }
    }
}
