//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Couchsurfing.ADL.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class PrivacySettings
    {
        public int Id { get; set; }
        public int User_profile_id { get; set; }
        public bool Settings { get; set; }
    
        public virtual UserProfile UserProfile { get; set; }
    }
}
