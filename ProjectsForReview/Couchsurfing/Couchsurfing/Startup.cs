﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Couchsurfing.Startup))]
namespace Couchsurfing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}