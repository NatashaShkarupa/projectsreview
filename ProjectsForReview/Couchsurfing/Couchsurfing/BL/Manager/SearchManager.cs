﻿using System.Collections.Generic;
using System.Linq;
using Couchsurfing.ADL.Repositories;
using Couchsurfing.BL.ViewModels;

namespace Couchsurfing.BL.Manager
{
    public class SearchManager
    {
        UserProfileRepository _user = new UserProfileRepository();
        SearchRepository _search = new SearchRepository();
        UserHomeRepository _home=new UserHomeRepository();

        public IEnumerable<SearchModel> GetListByName(string searchString)
        {
            return _search.SearchByName(searchString).Select(userAbout => new SearchModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,
                FirstName = userAbout.FirstName,
                SecondName = userAbout.SecondName,
                User_status = userAbout.User_status,
                Languages = userAbout.Languages,
                Birthday = userAbout.Birthday,
                Avatar = userAbout.Avatar
            });
        }

        public IEnumerable<SearchModel> GetListByLiving(string country, string city)
        {
            return _search.SearchByLiving(country, city).Select(userAbout => new SearchModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,
                FirstName = userAbout.FirstName,
                SecondName = userAbout.SecondName,
                User_status = userAbout.User_status,
                Languages = userAbout.Languages,
                Birthday = userAbout.Birthday,
                Avatar = userAbout.Avatar
            });
        }

        public IEnumerable<SearchModel> GetListAllSearch(SearchModel model)
        {
            int id = model.Sleeping_arrangement_id;
            string status = model.User_status;
            string sex = model.sex;
            int number = model.max_number;
            string language = model.Languages;
            int from = model.fromyear;
            int to = model.toage;

            return _search.SearchByAll(id, status, sex, number, language, from, to).Select(userAbout => new SearchModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,
                FirstName = userAbout.FirstName,
                SecondName = userAbout.SecondName,
                User_status = userAbout.User_status,
                Languages = userAbout.Languages,
                Birthday = userAbout.Birthday,
                Avatar = userAbout.Avatar
            });
        }

        public IEnumerable<SearchModel> GetListAllSearchWithName(string searchString, SearchModel model)
        {
            int id = model.Sleeping_arrangement_id;
            string status = model.User_status;
            string sex = model.sex;
            int number = model.max_number;
            string language = model.Languages;
            int from = model.fromyear;
            int to = model.toage;

            return _search.SearchByAllWithName(searchString, id, status, sex, number, language, from, to).Select(userAbout => new SearchModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,
                FirstName = userAbout.FirstName,
                SecondName = userAbout.SecondName,
                User_status = userAbout.User_status,
                Languages = userAbout.Languages,
                Birthday = userAbout.Birthday,
                Avatar = userAbout.Avatar
            });
            //return _search.SearchByAllWithName(searchString, model.Sleeping_arrangement_id, model.User_status, model.sex, model.max_number, model.Languages, model.fromyear, model.toage).Select(userAbout => new SearchModel
            //{
            //    Id = userAbout.Id,
            //    UserRegistration_id = userAbout.UserRegistration_id,
            //    FirstName = userAbout.FirstName,
            //    SecondName = userAbout.SecondName,
            //    User_status = userAbout.User_status,
            //    Languages = userAbout.Languages,
            //    Birthday = userAbout.Birthday,
            //    Avatar = userAbout.Avatar
            //});
        }

        public IEnumerable<Arangment> GetListArangment()
        {
            return _home.GetListSleepingArrangement().Select(miscellaneousModel => new Arangment
            {
                Id = miscellaneousModel.Id,
                Label = miscellaneousModel.Label
            });
        }
    }
}