﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Couchsurfing.ADL.Repositories;
using Couchsurfing.ADL.Model;
using Couchsurfing.BL.ViewModels;

namespace Couchsurfing.BL.Manager
{
    public class HomeManager
    {
        UserHomeRepository _home = new UserHomeRepository();
        UserProfileRepository _user = new UserProfileRepository();
        HomeModel _homemodel = new HomeModel();

        public IEnumerable<HomeModelView> GetList(string idRegistration)
        {
            return _home.GetListById(idRegistration).Select(homeModel => new HomeModelView
            {
                Id = homeModel.Id,
                User_profile_id = homeModel.User_profile_id,
                Sleeping_arrangement = _home.GetArangment(idRegistration),
                Description_of_sleeping_arrangement = homeModel.Description_of_sleeping_arrangement,
                Roommate_situation = homeModel.Roommate_situation,
                Share_with_guests = homeModel.Share_with_guests,
                Additional_information = homeModel.Additional_information,
                Max_number_guist = homeModel.Max_number_guist,
                Prefer_sex = homeModel.Prefer_sex
            });
        }

        public HomeModel GetListModel(string idRegistration)
        {
            int id = _user.GetUserIdByIdRegistration(idRegistration);
            var home = _home.GetById(id);
            if (home != null)
            {
                _homemodel.Id = home.Id;
                _homemodel.User_profile_id = home.User_profile_id;
                _homemodel.Sleeping_arrangement_id = home.Sleeping_arrangement_id;
                _homemodel.Description_of_sleeping_arrangement = home.Description_of_sleeping_arrangement;
                _homemodel.Roommate_situation = home.Roommate_situation;
                _homemodel.Share_with_guests = home.Share_with_guests;
                _homemodel.Additional_information = home.Additional_information;
                _homemodel.Max_number_guist = home.Max_number_guist;
                _homemodel.Prefer_sex = home.Prefer_sex;
                return _homemodel;
            }
            return null;
        }

        public void Edit(string id, HomeModel model)
        {
            var idUser = _user.GetUserIdByIdRegistration(id);
            var curentUserData = _home.GetById(idUser);
            UserHome home = new UserHome();
            home.Id = model.Id;
            home.User_profile_id = idUser;
            home.Sleeping_arrangement_id = model.Sleeping_arrangement_id;
            home.Description_of_sleeping_arrangement = model.Description_of_sleeping_arrangement;
            home.Roommate_situation = model.Roommate_situation;
            home.Share_with_guests = model.Share_with_guests;
            home.Additional_information = model.Additional_information;
            home.Max_number_guist = model.Max_number_guist;
            home.Prefer_sex = model.Prefer_sex;

            //model.Miscellaneous

            _home.AddEditHome(home, idUser);
            _user.Save();
        }

        public IEnumerable<MiscellaneousModel> GetListMiscellaneous()
        {
            return _home.GetListMiscellaneous().Select(miscellaneousModel => new MiscellaneousModel
            {
                Id = miscellaneousModel.Id,
                Label = miscellaneousModel.Label
            });
        }

        public IEnumerable<Arangment> GetListArangment()
        {
            return _home.GetListSleepingArrangement().Select(miscellaneousModel => new Arangment
            {
                Id = miscellaneousModel.Id,
                Label = miscellaneousModel.Label
            });
        }
    }
}