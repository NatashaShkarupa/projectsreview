﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Couchsurfing.ADL.Repositories;
using Couchsurfing.ADL.Model;
using Couchsurfing.BL.ViewModels;

namespace Couchsurfing.BL.Manager
{
    public class TripManager
    {
        private TripRepository _tripRepository = new TripRepository();

        public int GetUserIdByIdRegistration(string id)
        {
            return _tripRepository.GetUserIdByIdRegistration(id);

        }
        public IEnumerable<ToDoListModel> GetToDoList(int id)
        {

            return _tripRepository.GetToDoList(id).Select(obj => new ToDoListModel
            {
                Id = obj.Id,
                Trip_id = obj.Trip_id,
                ToDoList_text = obj.ToDoList_text
            });
        }
        public IEnumerable<TripModel> GetList(string id)
        {

            return _tripRepository.GetList(id).Select(obj => new TripModel
            {
                Id = obj.Id,
                User_profile_id = obj.User_profile_id, //бд поменять
                Description = obj.Description,
                Destination = obj.Destination,
                Departure_date = obj.Departure_date,
                Arrival_date = obj.Arrival_date,
                LivingPlace = FindByIdLivingUser(id),
                regID = id
            });
        }

        public void Add(TripModel tripModel, string id)
        {
            int temp = _tripRepository.GetUserIdByIdRegistration(id);
            Trip trip = new Trip();
            //trip.Id = tripModel.Id;
            trip.User_profile_id = temp; //бд поменять
            trip.Description = tripModel.Description;
            trip.Destination = tripModel.Destination;
            trip.Departure_date = tripModel.Departure_date;
            trip.Arrival_date = tripModel.Arrival_date;
            _tripRepository.Add(trip);
            _tripRepository.Save();
        }
        public void AddToDoList(int id)
        {
            _tripRepository.AddToDoList(id);
        }
        public void AddToDoList(ToDoListModel obj)
        {
            ToDoList td = new ToDoList() { Trip_id = obj.Trip_id, ToDoList_text = obj.ToDoList_text};
            _tripRepository.AddToDoList(td);
        }
        public string FindByIdLivingUser(string idRegistration)
        {
            return _tripRepository.FindByIdLivingUser(idRegistration);
        }
        public void DeleteLIst(int id, int tripid)
        {
            _tripRepository.DeleteList(id, tripid);
        }
    }
}