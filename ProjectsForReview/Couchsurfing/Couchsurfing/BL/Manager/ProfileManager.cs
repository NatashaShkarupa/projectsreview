﻿using System.Collections.Generic;
using System.Linq;
using Couchsurfing.ADL.Repositories;
using Couchsurfing.ADL.Model;
using Couchsurfing.BL.ViewModels;

namespace Couchsurfing.BL.Manager
{
    public class ProfileManager
    {
        CountryRepository _country = new CountryRepository();
        CityRepository _city = new CityRepository();
        UserProfileRepository _user = new UserProfileRepository();
        UserHomeRepository _home = new UserHomeRepository();
        LivingPlaceRepository _livingPlace = new LivingPlaceRepository();
        AboutModel _model = new AboutModel();

        public IEnumerable<AboutModel> GetListOverView(string idRegistration)
        {
            string livingPlace = _user.FindByIdLivingUser(idRegistration);
            return _user.GetListByIdRegistration(idRegistration).Select(userAbout => new AboutModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,
                FirstName=userAbout.FirstName,
                SecondName=userAbout.SecondName,
                Living_place = livingPlace,
                User_status = userAbout.User_status,
                Languages = userAbout.Languages,
                Sex= userAbout.Sex,
                Birthday = userAbout.Birthday,
                Education = userAbout.Education,
                Ocupation = userAbout.Ocupation,
                //Rating=userAbout.Rating,
                Skype = userAbout.Skype,
                Avatar=userAbout.Avatar
            });
       }

        public IEnumerable<PhotosModel> GetListOfPhotos(string idRegistration)
        {
            return _user.GetListPhotosByIdRegistration(idRegistration).Select(userAbout => new PhotosModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.User_Id,
                Date = userAbout.Photo_date,
                Avatar = userAbout.Photo_file
            });
        }

        public IEnumerable<AboutModel> GetListAbout(string idRegistration)
        {
            string livingPlace = _user.FindByIdLivingUser(idRegistration);
            return _user.GetListByIdRegistration(idRegistration).Select(userAbout => new AboutModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,              
                About_yourself= userAbout.About_yourself,
                Participation_reason= userAbout.Participation_reason,
                Interests= userAbout.Interests,
                Favorite_stuff= userAbout.Favorite_stuff,
                Amazing_thing= userAbout.Amazing_thing,
                Teach_learn_share= userAbout.Teach_learn_share,
                Share_with_hosts= userAbout.Share_with_hosts,
                Response_rate= userAbout.Response_rate,
            }).Where(x => x.UserRegistration_id == idRegistration);
        }

        public IEnumerable<AboutModel> GetAllUser(string idRegistration)
        {
              string livingPlace = _user.FindByIdLivingUser(idRegistration);
            return _user.GetList().Select(userAbout => new AboutModel
            {
                Id = userAbout.Id,
                UserRegistration_id = userAbout.UserRegistration_id,
                FirstName = userAbout.FirstName,
                SecondName = userAbout.SecondName,
                Living_place = livingPlace,
                User_status = userAbout.User_status,
                Languages = userAbout.Languages,
                Sex = userAbout.Sex,
                Birthday = userAbout.Birthday,
                Education = userAbout.Education,
                Ocupation = userAbout.Ocupation,
                //Rating=userAbout.Rating,
                Skype = userAbout.Skype,
                Avatar = userAbout.Avatar,
                About_yourself = userAbout.About_yourself,
                Participation_reason = userAbout.Participation_reason,
                Interests = userAbout.Interests,
                Favorite_stuff = userAbout.Favorite_stuff,
                Amazing_thing = userAbout.Amazing_thing,
                Teach_learn_share = userAbout.Teach_learn_share,
                Share_with_hosts = userAbout.Share_with_hosts,
                Response_rate = userAbout.Response_rate,
            }).Where(x => x.UserRegistration_id == idRegistration);
        }

        public string GetLivingForMap(string idRegistration)
        {
            string livingPlace = _user.FindByIdLivingUser(idRegistration);
            return livingPlace;
        }

        public AboutModel GetAllUserModel(string idRegistration)
        {
            int id=_user.GetUserIdByIdRegistration(idRegistration);
            var user = _user.GetById(id);
            _model.Id = user.Id;
            _model.UserRegistration_id = user.UserRegistration_id;
            _model.FirstName = user.FirstName;
            _model.SecondName = user.SecondName;
            //_model.Living_place = user.Living_place_id;
            _model.User_status = user.User_status;
            _model.Languages = user.Languages;
            _model.Sex = user.Sex;
            _model.Birthday = user.Birthday;
            _model.Education = user.Education;
            _model.Ocupation = user.Ocupation;
            //Rating=userAbout.Rating,
            _model.Skype = user.Skype;
            _model.Avatar = user.Avatar;
            _model.About_yourself = user.About_yourself;
            _model.Participation_reason = user.Participation_reason;
            _model.Interests = user.Interests;
            _model.Favorite_stuff = user.Favorite_stuff;
            _model.Amazing_thing = user.Amazing_thing;
            _model.Teach_learn_share = user.Teach_learn_share;
            _model.Share_with_hosts = user.Share_with_hosts;
            _model.Response_rate = user.Response_rate;
            return (_model);
        }

        public IEnumerable<HomeModel> GetListHome(string idRegistration)
        {
            int id=_home.GetId(idRegistration);
            return _home.GetList().Select(user => new HomeModel
            {
                Id = user.Id,
                User_profile_id = user.User_profile_id,
                Sleeping_arrangement_id= user.Sleeping_arrangement_id,
                Description_of_sleeping_arrangement= user.Description_of_sleeping_arrangement,
                Roommate_situation=user.Roommate_situation,
                Share_with_guests=user.Share_with_guests,
                Additional_information=user.Additional_information,
                Max_number_guist=user.Max_number_guist,
                Prefer_sex=user.Prefer_sex,
                //foreing key
            }).Where(x => x.Id == id);
        }

        public void update(AboutModel user, string id)
        {
            int userid = _user.GetId(id);
            var newuser = _user.GetById(userid);
            newuser.Avatar = user.Avatar;
            _user.Edit(newuser);
            _user.Save();
        }

        public void AddPhotos(PhotosModel user, string id)
        {
            int userid = _user.GetId(id);
            Photo photo = new Photo();
            photo.User_Id = userid;
            photo.Photo_date = user.Date;
            photo.Photo_file = user.Avatar;
            _user.AddPhotos(photo);
            _user.Save();
        }

        public void Edit(string id, AboutModel userAbout)
        {
            var idUser = _user.GetUserIdByIdRegistration(id);
            var curentUserData = _user.GetById(idUser);
            UserProfile user = new UserProfile();
            user.Id = userAbout.Id;
            user.UserRegistration_id = userAbout.UserRegistration_id;
            user.User_status = curentUserData.User_status;
            user.Languages = userAbout.Languages;
            user.Sex = curentUserData.Sex;
            user.Birthday = curentUserData.Birthday;
            user.Education = userAbout.Education;
            user.Ocupation = userAbout.Ocupation;
            user.About_yourself = userAbout.About_yourself;
            user.Participation_reason = userAbout.Participation_reason;
            user.Interests = userAbout.Interests;
            user.Favorite_stuff = userAbout.Favorite_stuff;
            user.Amazing_thing = userAbout.Amazing_thing;
            user.Teach_learn_share = userAbout.Teach_learn_share;
            user.Share_with_hosts = userAbout.Share_with_hosts;
            user.Response_rate = userAbout.Response_rate;

            user.FirstName = curentUserData.FirstName;
            user.SecondName = curentUserData.SecondName;
            user.Living_place_id = curentUserData.Living_place_id;
            user.Avatar = curentUserData.Avatar;
            user.Skype = curentUserData.Skype;
            _user.Edit(user, id);
            _user.Save();
        }

        public void AddCountry(string country)
        {
            Country newcountry = new Country();
            newcountry.Label = country;
            _country.AddCategory(newcountry);
            _user.Save();
        }

        public void AddSity(string city)
        {
            City newcity = new City();
            newcity.Label = city;
            _city.AddCategory(newcity);
            _user.Save();
        }
    }
}