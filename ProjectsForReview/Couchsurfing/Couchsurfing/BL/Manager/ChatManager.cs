﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Couchsurfing.ADL.Repositories;
using Couchsurfing.ADL.Model;
using Couchsurfing.BL.ViewModels;

namespace Couchsurfing.BL.Manager
{
    public class ChatManager:BaseRepository
    {
        private UserProfileRepository _user = new UserProfileRepository();
    

        public void Add(ChatUser userChat, ChatMessage chatMessager, string registrationId)
        {
            int id=_user.GetId(registrationId);
            string firstNameUser = _user.GetName(id);

            Messager _msg = new Messager();
            _msg.First_user_profile_id = id;
            _msg.Second_user_profile_id = 0;
            userChat.FirstNameUser = firstNameUser;

            _msg.Message_text = chatMessager.Message_text;
            _msg.Message_date = chatMessager.Message_date;

            //_messager.AddCategory(_msg);
            //_messager.Save();
        }

        public string GetName(string id)
        {
            return _user.GetUserNameByIdRegistration(id);
        }
        
        public byte[] Avatar(string id)
        {
           var a=_user.GetAvatarByIdRegistration(id);
            return a;
        }

        public byte[] GetAvatarByIdRegistration(string id)
        {
            using (var context = GetContext())
            {
                var a = context.UserProfile.Where(x => x.UserRegistration_id == id).Select(x => x.Avatar).FirstOrDefault();
                return a;
            }
        }
    }
}