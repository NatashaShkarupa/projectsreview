﻿using System.Collections.Generic;
using System.Linq;
using Couchsurfing.ADL.Repositories;
using Couchsurfing.ADL.Model;
using Couchsurfing.BL.ViewModels;

namespace Couchsurfing.BL.Manager
{
    public class NecessaryDataRegistrationManager
    {
        private UserProfileRepository _user = new UserProfileRepository();
        private CountryRepository _country = new CountryRepository();
        private CityRepository _city = new CityRepository();
        private LivingPlaceRepository _livingPlace = new LivingPlaceRepository();
        NecessaryDataRegistrationModel _model = new NecessaryDataRegistrationModel();

        public IEnumerable<NecessaryDataRegistrationModel> GetListOfUsers()
        {
            return _user.GetList().Select(necessaryuser => new NecessaryDataRegistrationModel
            {
                UserRegistration_id = necessaryuser.UserRegistration_id,
                Sex = necessaryuser.Sex,
                Birthday = necessaryuser.Birthday,
                FirstName = necessaryuser.FirstName,
                SecondName = necessaryuser.SecondName,
            });
        }

        public void Add(NecessaryDataRegistrationModel necessaryuser, string country, string city)
        {
            LivingPlace livingPlace = new LivingPlace();
            livingPlace.Country_id = _country.FindByName(country);
            livingPlace.City_id = _city.FindByName(city);
            livingPlace.UAddress_id = necessaryuser.Address;
            _livingPlace.AddCategory(livingPlace);

            UserProfile newuser = new UserProfile();
            newuser.UserRegistration_id = necessaryuser.UserRegistration_id;
            newuser.Sex = necessaryuser.Sex;
            newuser.Birthday = necessaryuser.Birthday;
            newuser.User_status = necessaryuser.User_status;
            newuser.Living_place_id = livingPlace.Id;
            newuser.FirstName = necessaryuser.FirstName;
            newuser.SecondName = necessaryuser.SecondName;
            _user.AddCategory(newuser);
            _user.Save();
        }

        //public void AddCountry(string country)
        //{
        //    Country newcountry = new Country();
        //    newcountry.Label = country;
        //    _country.AddCategory(newcountry);
        //    _user.Save();
        //}

        //public void AddSity(string city)
        //{
        //    City newcity = new City();
        //    newcity.Label = city;
        //    _city.AddCategory(newcity);
        //    _user.Save();
        //}

        public void Edit(string id, NecessaryDataRegistrationModel userAbout, string city, string country)
        {
            var idUser = _user.GetUserIdByIdRegistration(id);
            var curentUserData = _user.GetById(idUser);

            UserProfile user = new UserProfile();
            user.Id = idUser;
            user.UserRegistration_id = userAbout.UserRegistration_id;
            user.Sex = userAbout.Sex;
            user.Birthday = userAbout.Birthday;
            user.FirstName = userAbout.FirstName;
            user.SecondName = userAbout.SecondName;           
            user.Skype = userAbout.Skype;
            user.User_status = userAbout.User_status;
            _user.UpdateUserLiving(userAbout.Address, city, country, userAbout.UserRegistration_id);

            user.Living_place_id = _livingPlace.FindIdByCountryCityString(userAbout.Address, country, city);

            user.Languages = curentUserData.Languages;
            user.Education = curentUserData.Education;
            user.Ocupation = curentUserData.Ocupation;
            user.About_yourself = curentUserData.About_yourself;
            user.Participation_reason = curentUserData.Participation_reason;
            user.Interests = curentUserData.Interests;
            user.Favorite_stuff = curentUserData.Favorite_stuff;
            user.Amazing_thing = curentUserData.Amazing_thing;
            user.Teach_learn_share = curentUserData.Teach_learn_share;
            user.Share_with_hosts = curentUserData.Share_with_hosts;
            user.Response_rate = curentUserData.Response_rate;
            user.Avatar = curentUserData.Avatar;
            _user.Edit(user, id);
            _user.Save();

        }

        public NecessaryDataRegistrationModel GetSettingsUser(string idRegistration)
        {
            int id = _user.GetUserIdByIdRegistration(idRegistration);
            var user = _user.GetById(id);
            var living = _user.FindByIdLivingUser(idRegistration);
            _model.Id = user.Id;
            _model.UserRegistration_id = user.UserRegistration_id;
            _model.FirstName = user.FirstName;
            _model.SecondName = user.SecondName;
            _model.Living_place = living;
            _model.User_status = user.User_status;
            _model.Sex = user.Sex;
            //var date = Convert.ToDateTime(user.Birthday);
            //_model.Birthday = date.Date.ToString("dd:MM:yyyy");
            _model.Skype = user.Skype;
            return (_model);
        }
    }
}