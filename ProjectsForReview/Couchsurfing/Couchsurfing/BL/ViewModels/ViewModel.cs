﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class ViewModel
    {
        public IEnumerable<HomeModelView> UserHome { get; set; }
        public IEnumerable<AboutModel> UserProfile { get; set; }
    }
}