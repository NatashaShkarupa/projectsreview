﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class SearchModel
    {
        public int Id { get; set; }
        public string UserRegistration_id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte[] Avatar { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }

        public string User_status { get; set; }
        public string Languages { get; set; }
        public int Sleeping_arrangement_id { get; set; }
        public string sex { get; set; }
        public int max_number { get; set; }
        public int fromyear { get; set; }
        public int toage { get; set; }
    }
}