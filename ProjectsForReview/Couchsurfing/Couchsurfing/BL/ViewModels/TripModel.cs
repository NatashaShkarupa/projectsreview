﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class TripModel
    {

        public int Id { get; set; }
        public int User_profile_id { get; set; }
        [Display(Name = "Место назначения")]
        public string Destination { get; set; }
        [Display(Name = "Дата прибытия")]
        public System.DateTime Arrival_date { get; set; }
        [Display(Name = "Дата отбытия")]
        public System.DateTime Departure_date { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Место жительства")]
        public string LivingPlace { get; set; }
        public string regID { get; set; }
    }
}