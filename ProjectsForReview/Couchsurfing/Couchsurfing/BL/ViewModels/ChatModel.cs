﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class ChatModel
    {
        public List<ChatUser> Users;

        public List<ChatMessage> Messages;

        public ChatModel()
        {
            Users = new List<ChatUser>();
            Messages = new List<ChatMessage>();
        }
    }
    public class ChatUser
    {
        public string ConnectionId { get; set; }
        public string FirstNameUser { get; set; }
        public byte[] Avatar { get; set; }
    }

    public class ChatMessage
    {
        public ChatUser User { get; set; }
        public string Message_text = " ";
        public DateTime Message_date = DateTime.Now;

    }
}