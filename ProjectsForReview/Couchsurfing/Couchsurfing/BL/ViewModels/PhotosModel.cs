﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class PhotosModel
    {
        public int Id { get; set; }
        public int? UserRegistration_id { get; set; }
        public byte[] Avatar { get; set; }
        [DisplayFormat(DataFormatString = "{dd.mm.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
    }
}