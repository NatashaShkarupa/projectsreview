﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Couchsurfing.BL.ViewModels
{
    public class UserProfileModel
    {
        public int Id { get; set; }
        public string UserRegistration_id { get; set; }
        public string Living_place { get; set; }
        [Display(Name = "Status", ResourceType = typeof(Resources.Resource))]
        public string User_status { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Language", ResourceType = typeof(Resources.Resource))]
        public string Languages { get; set; }
        [Display(Name = "Sex", ResourceType = typeof(Resources.Resource))]
        public string Sex { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Education", ResourceType = typeof(Resources.Resource))]
        public string Education { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Ocupation", ResourceType = typeof(Resources.Resource))]
        public string Ocupation { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Birthday", ResourceType = typeof(Resources.Resource))]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> Birthday { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "About_yourself", ResourceType = typeof(Resources.Resource))]
        public string About_yourself { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Participation_reason", ResourceType = typeof(Resources.Resource))]
        public string Participation_reason { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Interests", ResourceType = typeof(Resources.Resource))]
        public string Interests { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Favorite_stuff", ResourceType = typeof(Resources.Resource))]
        public string Favorite_stuff { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Amazing_thing", ResourceType = typeof(Resources.Resource))]
        public string Amazing_thing { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Teach_learn_share", ResourceType = typeof(Resources.Resource))]
        public string Teach_learn_share { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Share_with_hosts", ResourceType = typeof(Resources.Resource))]
        public string Share_with_hosts { get; set; }

        public Nullable<int> Response_rate { get; set; }
        [Display(Name = "Skype", ResourceType = typeof(Resources.Resource))]
        public string Skype { get; set; }
        [Display(Name = "Rating", ResourceType = typeof(Resources.Resource))]
        public Nullable<int> Rating { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "SecondName", ResourceType = typeof(Resources.Resource))]
        public string SecondName { get; set; }
        public byte[] Avatar { get; set; }

        public Nullable<int> Sleeping_arrangement_id { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Description_of_sleeping_arrangement", ResourceType = typeof(Resources.Resource))]
        public string Description_of_sleeping_arrangement { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Roommate_situation", ResourceType = typeof(Resources.Resource))]
        public string Roommate_situation { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Share_with_guests", ResourceType = typeof(Resources.Resource))]
        public string Share_with_guests { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Additional_information", ResourceType = typeof(Resources.Resource))]
        public string Additional_information { get; set; }
        [Display(Name = "Max_number_guist", ResourceType = typeof(Resources.Resource))]
        public Nullable<int> Max_number_guist { get; set; }
        [Display(Name = "Sex", ResourceType = typeof(Resources.Resource))]
        public string Prefer_sex { get; set; }
    }
}