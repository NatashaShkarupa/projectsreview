﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class MiscellaneousModel
    {
        public int Id { get; set; }
        public int IdHome { get; set; }
        public string Label { get; set; }
        public bool Bool { get; set; }
    }
}