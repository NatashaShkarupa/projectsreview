﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Couchsurfing.BL.ViewModels
{
    public class NecessaryDataRegistrationModel
    {
        public int Id { get; set; }
        public string UserRegistration_id { get; set; }
        [Required]
        [Display(Name = "Sex", ResourceType = typeof(Resources.Resource))]
        public string Sex { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{dd.mm.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Birthday", ResourceType = typeof(Resources.Resource))]
        public Nullable<System.DateTime> Birthday { get; set; }
        [Required]
        [Display(Name = "Living_place", ResourceType = typeof(Resources.Resource))]
        public string Living_place { get; set; }
        //[Required]
        [Display(Name = "Status", ResourceType = typeof(Resources.Resource))]
        public string User_status { get; set; }
        [Required]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "SecondName", ResourceType = typeof(Resources.Resource))]
        public string SecondName { get; set; }
        [Display(Name = "Skype", ResourceType = typeof(Resources.Resource))]
        public string Skype { get; set; }
        //[RegularExpression(@"[а-яА-Я]+,[А-Яа-я]", ErrorMessage = "Некорретый город")]
        [Display(Name = "Address", ResourceType = typeof(Resources.Resource))]
        public string Address { get; set; }
    }
}