﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BL.ViewModels
{
    public class UserPreferencesModel
    {
        public int Id { get; set; }
        //[StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        //[Display(Name = "Max number of guests")]
        public int Max_number_guests { get; set; }
        public int Preffered_sex_id { get; set; }
        public int Exception_Id { get; set; }
    }
}
