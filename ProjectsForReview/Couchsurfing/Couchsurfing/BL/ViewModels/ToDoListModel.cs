﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class ToDoListModel
    {
        public int Id { get; set; }
        public int Trip_id { get; set; }
        public string ToDoList_text { get; set; }
        public int Status { get; set; }
        public int ToDoList_order { get; set; }

    }
}