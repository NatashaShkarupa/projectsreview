﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Couchsurfing.BL.ViewModels
{
    public class HomeModelView
    {
        public int Id { get; set; }
        public int User_profile_id { get; set; }
        [Display(Name = "Sleepingarrangement", ResourceType = typeof(Resources.Resource))]
        public string Sleeping_arrangement { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Description_of_sleeping_arrangement", ResourceType = typeof(Resources.Resource))]
        public string Description_of_sleeping_arrangement { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Roommate_situation", ResourceType = typeof(Resources.Resource))]
        public string Roommate_situation { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Share_with_guests", ResourceType = typeof(Resources.Resource))]
        public string Share_with_guests { get; set; }
        [StringLength(50, MinimumLength = 5, ErrorMessageResourceName = "StringLengthMessage", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Display(Name = "Additional_information", ResourceType = typeof(Resources.Resource))]
        public string Additional_information { get; set; }
        [Display(Name = "Max_number_guist", ResourceType = typeof(Resources.Resource))]
        public Nullable<int> Max_number_guist { get; set; }
        [Display(Name = "PreferSex", ResourceType = typeof(Resources.Resource))]
        public string Prefer_sex { get; set; }
    }
}