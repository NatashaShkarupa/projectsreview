﻿(function () {

    // Ссылка на автоматически-сгенерированный прокси хаба
    var chat = $.connection.chatHub;

    // Функция, вызываемая при подключении нового пользователя
    chat.client.onConnected = function (id, userName, avatar, allUsers) {

    //    { 
    //        var base64 = Convert.ToBase64String(avatar.Avatar);
    //    var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
    //}

        a=function(avatar){
            return avatar;
        }

    // установка в скрытых полях имени и id текущего пользователя
    $('.hdId').val(id);
    $('.username').val(userName);

    // Добавление всех пользователей
    for (i = 0; i < allUsers.length; i++) {

        AddUser(allUsers[i].ConnectionId, allUsers[i].Name);
    }
}

    ////Добавление нового пользователя
    //function AddUser(id, name) {

    //    var userId = $('.hdId').val();

    //    if (userId != id) {

    //        $(".chatusers").append('<p id="' + id + '"><b>' + name + '</b></p>');
    //    }
    //}

    //// Добавляем нового пользователя
    //chat.client.onNewUserConnected = function (id, name) {

    //    AddUser(id, name);
    //}

    //// Удаляем пользователя
    //chat.client.onUserDisconnected = function (id, userName) {

    //    $('.' + id).remove();
    //}

    var Message;
Message = function (arg) {
    this.text = arg.text, this.message_side = arg.message_side;
    this.draw = function (_this) {
        return function () {
            var $message;
            $message = $($('.message_template').clone().html());
            $message.addClass(_this.message_side).find('.text').html(_this.text);
            $('.messages').append($message);
            return setTimeout(function () {
                return $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
};

$(function () {
    var getMessageText, message_side, sendMessage;
    message_side = 'right';
    getMessageText = function () {
        var $message_input;
        $message_input = $('.message_input');
        return $message_input.val();
    };
       
    sendMessage = function (text) {
        var $messages, message;
        if (text.trim() === '') {
            return;
        }
        debugger;
        $('.message_input').val('');
        $messages = $('.messages');
        message_side = message_side === 'left' ? 'right' : 'left';
        message = new Message({
            text: text,
            message_side: message_side
        });
        message.draw();
        return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);

    };

    curenTime = function () {
        var currentdate = new Date();
        var time =  currentdate.getHours() + ":"
                    + currentdate.getMinutes()
        return time;
    }

    avatar = function () {
        var avatar = a();
        return document.getElementById("ItemPreview").src = "data:image/png;base64," + avatar;
    }

    // Объявление функции, которая хаб вызывает при получении сообщений
    chat.client.addMessage = function (name, msg) {
        var time = curenTime();
        return sendMessage(name + " " + time + '<br />' + msg);
    };

    // Открываем соединение
    $.connection.hub.start().done(function () {

        $('.send_message').click(function (e) {
            chat.server.send($('.username').val(), $('.message_input').val());
        });

        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                chat.server.send($('.username').val(), $('.message_input').val());
            }
        });

        // обработка логина
        $(".send_message").click(function () {
            chat.server.connect();
        });
    });
});
}.call(this));