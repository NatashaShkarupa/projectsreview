﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Collections.Specialized;
using System.IO;
using System.Drawing;
using Couchsurfing.BL.ViewModels;
using Couchsurfing.Filters;
using Microsoft.AspNet.Identity;
using Couchsurfing.BL.Manager;

namespace Couchsurfing.Controllers
{
    public class ProfileController : Controller
    {
        ProfileManager _profileManaager = new ProfileManager();
        AboutModel _aboutModel = new AboutModel();
        UserProfileModel _userProfileModel = new UserProfileModel();
        NecessaryDataRegistrationModel _necessaryDataRegistrationModel = new NecessaryDataRegistrationModel();
        NecessaryDataRegistrationManager _necessaryDataRegistrationManager = new NecessaryDataRegistrationManager();
        HomeModel _homeModel = new HomeModel();
        HomeManager _homeManager = new HomeManager();
        MiscellaneousModel _miscellaneousModel = new MiscellaneousModel();
        PhotosModel _photosModel = new PhotosModel();

        //static List<GoogleActions> gact = new List<GoogleActions>();

        //public ActionResult AutocompleteSearch(string term)
        //{
        //    GoogleActions ggl = new GoogleActions();
        //    ggl.FindCities(term);
        //    for (int i = 0; i < 5; i++)
        //        gact.Add(new GoogleActions() { city = ggl.cities[i] });
        //    if (term != null)
        //    {
        //        var models = gact.Where(a => a.city != null && a.city.Contains(term))
        //                        .Select(a => new { value = a.city })
        //                        .Distinct();
        //        return Json(models, JsonRequestBehavior.AllowGet);
        //    }
        //    else return Content("Error, string is null");

        //}

        public ActionResult UserProfile1()
        {
            if (_aboutModel.Avatar != null)
            {
                ShowAvatar(_aboutModel.Avatar);
            }

            string id = User.Identity.GetUserId();
            var living=_profileManaager.GetLivingForMap(id);

            var viewmodel = new ViewModel();
            viewmodel.UserHome = _homeManager.GetList(id);
            viewmodel.UserProfile = _profileManaager.GetAllUser(id);

            if (ModelState.IsValid)
            {
                return View(viewmodel);
            }
            return View(viewmodel);
        }

        
        [HttpPost]
        public ActionResult _EditHome()
        {
            var model2 = _homeManager.GetListArangment().
                Select(x => new SelectListItem
                {
                    Text = x.Label,
                    Value = x.Id.ToString()
                }).ToList();
            ViewBag.Arangment = model2;

            string id = User.Identity.GetUserId();
            _homeModel = _homeManager.GetListModel(id);
            if (_homeModel != null)
            {
                return PartialView(_homeModel);
            }
            return PartialView("_EditHome");
        }

        [HttpGet]
        public ActionResult _EditHome([Bind(Include = "Sleeping_arrangement_id,Description_of_sleeping_arrangement,Roommate_situation,Share_with_guests,Additional_information,Max_number_guist,Prefer_sex")] HomeModel _homeModel)
        {
            if (ModelState.IsValid)
            {
                string id = User.Identity.GetUserId();
                _homeManager.Edit(id, _homeModel);
                return RedirectToAction("UserProfile1");
            }
            return PartialView("_EditHome");
        }
       
        [HttpGet]
        public ActionResult _EditProfilePhotos()
        {
            string id = User.Identity.GetUserId();
            var model = _profileManaager.GetListOfPhotos(id);
            if (_photosModel.Avatar != null)
            {
                ShowAvatar(_photosModel.Avatar);
            }
            return PartialView(model);
        }

        [HttpPost]
        public void _EditProfilePhotos(HttpPostedFileBase file)
        {
            string id = User.Identity.GetUserId();
            if (file != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    _photosModel.Date = DateTime.Now;
                    _photosModel.Avatar = array;
                    _profileManaager.AddPhotos(_photosModel, id);
                }
            }
            
        }


        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase file)
        {
            string id = User.Identity.GetUserId();
            if (file != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    _aboutModel.Avatar = array;
                    _profileManaager.update(_aboutModel, id);
                }
            }
            return View(_profileManaager.GetListOverView(id));
        }

        [HttpGet]
        public ActionResult Edit()
        {
            string id = User.Identity.GetUserId();
            if (_aboutModel.Avatar != null)
            {
                ShowAvatar(_aboutModel.Avatar);
            }
                return View(_profileManaager.GetListOverView(id));
        }

        [HttpPost]
        public ActionResult _EditUser()
        {
            string id = User.Identity.GetUserId();
            _aboutModel = _profileManaager.GetAllUserModel(id);
            return PartialView(_aboutModel);
        }

        [HttpGet]
        public ActionResult _EditUser([Bind(Include = "Id,UserRegistration_id,Languages,Education,Ocupation,About_yourself,Participation_reason,Interests,Favorite_stuff,Amazing_thing,Teach_learn_share,Share_with_hosts")] AboutModel _aboutModel)
        {
            string id = User.Identity.GetUserId();
            if (_aboutModel.Avatar != null)
            {
                ShowAvatar(_aboutModel.Avatar);
            }

            if (ModelState.IsValid)
            {
                _aboutModel.UserRegistration_id = id;
                _profileManaager.Edit(id, _aboutModel);
                return RedirectToAction("UserProfile1");

            }
            return View("Index");
        }

        public Image ShowAvatar(byte[] avatar)
        {
            MemoryStream stm = new MemoryStream(avatar);
            stm.Position = 0;
            return Image.FromStream(stm);
        }

        public ActionResult _AboutList()
        {
            string id = User.Identity.GetUserId();
            return PartialView(_profileManaager.GetListAbout(id));
        }
        
        public ActionResult _HomeList()
        {
            string id = User.Identity.GetUserId();
            return PartialView(_homeManager.GetList(id));
        }

        
        [HttpGet]
        public ActionResult EditSettings()
        {
            string id = User.Identity.GetUserId();
            _necessaryDataRegistrationModel = _necessaryDataRegistrationManager.GetSettingsUser(id);
          
            return View(_necessaryDataRegistrationModel);
        }

        [HttpPost]
        public ActionResult EditSettings([Bind(Include = "Id,UserRegistration_id,Sex,Birthday,User_status,Living_place,Address,FirstName,SecondName,Skype")] NecessaryDataRegistrationModel _necessaryDataRegistrationModel)
        {
            string id = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                _necessaryDataRegistrationModel.UserRegistration_id = id;
                Char delimiter = ',';
                String[] substrings = _necessaryDataRegistrationModel.Living_place.Split(delimiter);
                if (substrings.Length == 2)
                {
                    _necessaryDataRegistrationManager.Edit(id, _necessaryDataRegistrationModel, substrings[0], substrings[1]);
                }
                else
                {
                    _necessaryDataRegistrationManager.Edit(id, _necessaryDataRegistrationModel, substrings[0], substrings[1]);
                }
                return RedirectToAction("UserProfile1");

            }
            return View("UserProfile1");
        }

        public ActionResult _Miscellaneous([Bind(Include = "Label,Bool")] MiscellaneousModel _miscellaneousModel)
        {

            return RedirectToAction("_EditHome");
        }
    }
}
