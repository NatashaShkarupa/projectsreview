﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Couchsurfing.BL.ViewModels;
using Couchsurfing.Filters;
using Microsoft.AspNet.Identity;
using Couchsurfing.BL.Manager;
using System.Globalization;
using System.Threading;

namespace Couchsurfing.Controllers
{
    [Culture]
    public class HomeController : Controller
    {
        private NecessaryDataRegistrationManager _dataregistration = new NecessaryDataRegistrationManager();
        private NecessaryDataRegistrationModel _dataregistrationmodel = new NecessaryDataRegistrationModel();
        ProfileManager _profileManaager = new ProfileManager();
        AboutModel _aboutModel = new AboutModel();
        SearchManager _searchManager = new SearchManager();
        SearchModel _searchModel = new SearchModel();
        HomeModel _homeModel = new HomeModel();
        HomeManager _homeManager = new HomeManager();

        public string CurrentLangCode { get; protected set; }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            if (requestContext.HttpContext.Request.Url != null)
            {
                var HostName = requestContext.HttpContext.Request.Url.Authority;
            }

            if (requestContext.RouteData.Values["lang"] != null && requestContext.RouteData.Values["lang"] as string != "null")
            {
                CurrentLangCode = requestContext.RouteData.Values["lang"] as string;

                var ci = new CultureInfo(CurrentLangCode);
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            base.Initialize(requestContext);
        }
   
        public ActionResult Index()
        {
            return View(); 
        }

        public ActionResult Chat()
        {
            return View();
        }

        public ActionResult Work_scheduleGrid()
        {
            return View(_dataregistration.GetListOfUsers());
        }

        [HttpGet]
        public ActionResult _CreateUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _CreateUser([Bind(Include = "Id,UserRegistration_id,Sex,Birthday,User_status,Living_place,FirstName,SecondName")] NecessaryDataRegistrationModel _dataregistrationmodel)
        {
            if (ModelState.IsValid)
            {
                Char delimiter = ',';
                String[] substrings = _dataregistrationmodel.Living_place.Split(delimiter);
                if (substrings.Length == 2)
                {
                    _dataregistrationmodel.UserRegistration_id = User.Identity.GetUserId();
                    _dataregistration.Add(_dataregistrationmodel, substrings[1], substrings[0]);
                    return RedirectToAction("Index");
                }
                else
                {
                    _dataregistrationmodel.UserRegistration_id = User.Identity.GetUserId();
                    _dataregistration.Add(_dataregistrationmodel, "Кременчуг", "Украина");
                    return View("Index");
                }
            }
            return View("Index");
        }

        [Culture]
        public ActionResult ChangeCulture(string lang)
        {
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            
            List<string> cultures = new List<string>() { "ru", "en" };
            if (!cultures.Contains(lang))
            {
                lang = "ru";
            }
 
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang;  
            else
            {

                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }


        public ActionResult _PhotoDrop()
        {
            string id = User.Identity.GetUserId();
            var model = _profileManaager.GetListOverView(id);
            return PartialView("_PhotoDrop", model);
        }

        public ActionResult SearchByName(string searchString)
        {
            if (ModelState.IsValid)
            {
                var model = _searchManager.GetListByName(searchString);
                return View(model);
            }
            return View("Index");
        }

        public ActionResult SearchByLiving(string searchString)
        {
            if (ModelState.IsValid)
            {
                Char delimiter = ',';
                String[] substrings = searchString.Split(delimiter);
                if (substrings.Length == 2)
                {
                    var model = _searchManager.GetListByLiving(substrings[1], substrings[0]);
                    return View(model);
                }
                else
                {
                    return View("Index");
                }
            }
            return View("Index");
        }


        [HttpGet]
        public ActionResult SearchByAll(string searchString)
        {
            if (ModelState.IsValid)
            {
                var model2 = _homeManager.GetListArangment().
                Select(x => new SelectListItem
                {
                    Text = x.Label,
                    Value = x.Id.ToString()
                }).ToList();
                ViewBag.Arangment = model2;
                TempData["searchString"] = searchString;
                return View("SearchByAll");
            }
            return View("Index");
        }

        [HttpPost]
        public ActionResult SearchByAll([Bind(Include = "Sleeping_arrangement_id, User_status, sex, max_number, Languages, fromyear, toage")]
        SearchModel _searchModel)
        {
            ViewBag.searchString = TempData["searchString"];
            if (ModelState.IsValid)
            {
                if (ViewBag.searchString == null)
                {
                    var model = _searchManager.GetListAllSearch(_searchModel);
                    return View("SearchByAllView",model);
                }
                if (ViewBag.searchString != null)
                {
                    var model2 = _searchManager.GetListAllSearchWithName(ViewBag.searchString, _searchModel);
                    return View("SearchByAllView", model2);
                }
            }
            return View("Index");
        }

        public ActionResult SearchByAllView(IEnumerable<SearchModel>  model)
        {
            if (ModelState.IsValid)
            {
                return View(model);
            }
            return View("Index");
        }

        //static List<GoogleActions> gact = new List<GoogleActions>();

        //public ActionResult AutocompleteSearch(string term)
        //{
        //    GoogleActions ggl = new GoogleActions();
        //    ggl.FindCities(term);
        //    gact.Clear();
        //    for (int i = 0; i < 5; i++)
        //        gact.Add(new GoogleActions() { city = ggl.cities[i] });
        //    if (term != null)
        //    {
        //        var models = gact.Where(a => a.city != null && a.city.Contains(term))
        //                        .Select(a => new { value = a.city })
        //                        .Distinct();
        //        return Json(models, JsonRequestBehavior.AllowGet);
        //    }
        //    else return Content("Error, string is null");

        //}
    }
}
