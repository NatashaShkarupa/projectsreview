

CREATE TABLE UserProfile 
(Id integer primary key NOT NULL identity, 
UserRegistration_id nvarchar(128) NOT NULL,
Living_place_id integer NOT NULL, 
User_status_id integer NOT NULL, 
Languages_id integer, 
FirstName nvarchar(max), 
LastName nvarchar(max), 
Sex_id integer NOT NULL, 
Education nchar(100), 
Ocupation nchar(100), 
Birthday date NOT NULL, 
About_yourself nvarchar(max), 
Participation_reason nvarchar(max), 
Interests nchar(100), 
Favorite_stuff nvarchar(max), 
Amazing_thing nvarchar(max), 
Teach_learn_share nvarchar(max), 
Share_with_hosts nvarchar(max),
Rating integer, 
Response_rate integer NOT NULL, 
Skype nchar(100),
Phone_number nchar(20)); 

CREATE TABLE UserPreferences 
(Id integer primary key NOT NULL identity,
Max_number_guests integer, 
Preffered_sex_id integer); 

CREATE TABLE Exception 
(Id integer primary key NOT NULL identity, 
Label nchar(50) NOT NULL); 

CREATE TABLE ExceptionConnection 
(Id integer primary key NOT NULL identity,
Exception_id integer NOT NULL, 
UserPreferences_id integer NOT NULL); 

CREATE TABLE UserHome 
(Id integer primary key NOT NULL identity, 
User_profile_id integer NOT NULL,
Sleeping_arrangement_id integer,
Description_of_sleeping_arrangement nvarchar(max),
Roommate_situation nvarchar(max),
Share_with_guests nvarchar(max),
Additional_information nvarchar(max));

CREATE TABLE Miscellaneous
(Id integer primary key NOT NULL identity, 
Label nchar(50) NOT NULL); 

CREATE TABLE MiscellaneousConnection
(Id integer primary key NOT NULL identity, 
User_home_id integer NOT NULL, 
Miscellaneous_id integer NOT NULL); 

CREATE TABLE UserReferences
(Id integer primary key NOT NULL identity, 
Reference_type_id integer NOT NULL, 
Reference_from_type_id integer NOT NULL,
Descriptions nvarchar(max) NOT NULL,
Reference_date date NOT NULL,
Reference_time time NOT NULL,
Userfor_profile_id  integer NOT NULL,
Userfrom_profile_id integer NOT NULL); 

CREATE TABLE ReferenceType
(Id integer primary key NOT NULL identity, 
Label nchar(10) NOT NULL); 

CREATE TABLE ReferenceFromType
(Id integer primary key NOT NULL identity, 
Label nchar(20) NOT NULL); 

CREATE TABLE SleepingArrangement
(Id integer primary key NOT NULL identity, 
Label nchar(30) NOT NULL); 

CREATE TABLE Sex
(Id integer primary key NOT NULL identity, 
Label nchar(10) NOT NULL); 

CREATE TABLE WeekDaysConnection 
(Id integer primary key NOT NULL identity, 
User_home_id integer NOT NULL, 
Week_days_name_id integer NOT NULL); 

CREATE TABLE WeekDaysName
(Id integer primary key NOT NULL identity, 
Label nchar(15) NOT NULL); 

CREATE TABLE UserStatus
(Id integer primary key NOT NULL identity, 
Label nchar(30) NOT NULL); 

CREATE TABLE Languages
(Id integer primary key NOT NULL identity, 
Label nchar(30) NOT NULL); 

CREATE TABLE LivingPlace
(Id integer primary key NOT NULL identity, 
User_profile_id integer NOT NULL,
City_id integer NOT NULL,
Country_id integer NOT NULL,
UAddress_id integer); 

CREATE TABLE Country
(Id integer primary key NOT NULL identity, 
Label nchar(30) NOT NULL); 

CREATE TABLE City
(Id integer primary key NOT NULL identity, 
Label nchar(30) NOT NULL); 

CREATE TABLE UAddress
(Id integer primary key NOT NULL identity, 
Label nchar(100) NOT NULL); 

CREATE TABLE Friends
(Id integer primary key NOT NULL identity, 
First_user_profile_id integer NOT NULL,
Second_user_profile_id integer NOT NULL,
Friends_date date NOT NULL,
Friends_time time NOT NULL); 

CREATE TABLE Photo
(Id integer primary key NOT NULL identity, 
User_profile_id integer NOT NULL,
Photo_type_id integer NOT NULL,
Photo_file nvarchar(max) NOT NULL,
Photo_date date NOT NULL,
Photo_time time NOT NULL); 

CREATE TABLE Messager
(Id integer primary key NOT NULL identity, 
First_user_profile_id integer NOT NULL,
Second_user_profile_id integer NOT NULL,
Message_text nvarchar(max) NOT NULL,
Message_date date NOT NULL,
Message_time time NOT NULL); 

CREATE TABLE Invitation
(Id integer primary key NOT NULL identity, 
First_user_profile_id integer NOT NULL,
Second_user_profile_id integer NOT NULL,
Invitation_date date NOT NULL,
Invitation_time time NOT NULL); 

CREATE TABLE PhotoType
(Id integer primary key NOT NULL identity, 
Label nchar(20) NOT NULL);

CREATE TABLE Trip 
(Id integer primary key NOT NULL identity, 
User_profile_id integer NOT NULL, 
Destination nchar(50) NOT NULL, 
Arrival_date date NOT NULL, 
Departure_date date NOT NULL, 
Description nvarchar(max)); 

CREATE TABLE TripItem 
(Id integer primary key NOT NULL identity, 
Trip_id integer NOT NULL, 
Trip_item_date date NOT NULL, 
Sity_name nchar(20) NOT NULL, 
Trip_item_order integer NOT NULL); 

CREATE TABLE TripWish 
(Id integer primary key NOT NULL identity, 
Trip_id integer NOT NULL, 
TripWish_text nvarchar(max), 
Link nvarchar(max) NOT NULL); 

CREATE TABLE ToDoList 
(Id integer primary key NOT NULL identity, 
Trip_id integer NOT NULL, 
ToDoList_text nvarchar(max), 
Status integer NOT NULL, 
ToDoList_order int NOT NULL); 

CREATE TABLE PrivacySettings 
(Id integer primary key NOT NULL identity, 
User_profile_id integer NOT NULL, 
Settings bit NOT NULL);