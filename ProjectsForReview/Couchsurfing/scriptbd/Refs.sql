 use CouchsurfingDatabase2

GO 
ALTER TABLE ExceptionConnection 
ADD CONSTRAINT FK_ExceptionConnection_Exception_id FOREIGN KEY (Exception_id) 
REFERENCES Exception (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE ExceptionConnection 
ADD CONSTRAINT FK_ExceptionConnection_UserPreferences_id FOREIGN KEY (UserPreferences_id) 
REFERENCES UserPreferences (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

GO 
ALTER TABLE LivingPlace 
ADD CONSTRAINT FK_LivingPlace_UserProfile_id FOREIGN KEY (User_profile_id) 
REFERENCES UserProfile (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE LivingPlace 
ADD CONSTRAINT FK_LivingPlace_City_id FOREIGN KEY (City_id) 
REFERENCES City (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE LivingPlace 
ADD CONSTRAINT FK_LivingPlace_Country_id FOREIGN KEY (Country_id) 
REFERENCES Country (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

 
GO 
ALTER TABLE LivingPlace 
ADD CONSTRAINT FK_LivingPlace_UAddress_id FOREIGN KEY (UAddress_id) 
REFERENCES UAddress (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE Photo 
ADD CONSTRAINT FK_Photo_UserProfile_id FOREIGN KEY (User_profile_id) 
REFERENCES UserProfile (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE UserReferences
ADD CONSTRAINT FK_UserReferences_Reference_type_id FOREIGN KEY (Reference_type_id) 
REFERENCES ReferenceType (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE UserReferences
ADD CONSTRAINT FK_UserReferences_Reference_from_type_id FOREIGN KEY (Reference_from_type_id) 
REFERENCES ReferenceFromType (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

GO 
ALTER TABLE UserReferences 
ADD CONSTRAINT FK_UserReferences_UserfromProfile_id FOREIGN KEY (Userfrom_profile_id) 
REFERENCES UserProfile (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE;
GO


GO 
ALTER TABLE UserPreferences
ADD CONSTRAINT FK_UserPreferences_Preffered_sex_id FOREIGN KEY (Preffered_sex_id) 
REFERENCES Sex (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE Friends
ADD CONSTRAINT FK_Friends_First_user_profile_id FOREIGN KEY (First_user_profile_id) 
REFERENCES UserProfile (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE Friends
ADD CONSTRAINT FK_Friends_Second_user_profile_id FOREIGN KEY (Second_user_profile_id) 
REFERENCES UserProfile (Id)
ON DELETE NO ACTION 
ON UPDATE NO ACTION; 
GO


GO 
ALTER TABLE MiscellaneousConnection
ADD CONSTRAINT FK_MiscellaneousConnection_User_home_id FOREIGN KEY (User_home_id) 
REFERENCES UserHome (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE MiscellaneousConnection
ADD CONSTRAINT FK_MiscellaneousConnection_Miscellaneous_id FOREIGN KEY (Miscellaneous_id) 
REFERENCES Miscellaneous (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO



GO 
ALTER TABLE UserHome
ADD CONSTRAINT FK_UserHome_Sleeping_arrangement_id FOREIGN KEY (Sleeping_arrangement_id) 
REFERENCES SleepingArrangement (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE UserHome
ADD CONSTRAINT FK_UserHome_User_profile_id FOREIGN KEY (User_profile_id) 
REFERENCES  UserProfile (Id)
ON DELETE NO ACTION 
ON UPDATE NO ACTION; 
GO




 
GO 
ALTER TABLE UserProfile
ADD CONSTRAINT FK_UserProfile_Living_place_id FOREIGN KEY (Living_place_id) 
REFERENCES LivingPlace (Id)
GO


GO 
ALTER TABLE UserProfile
ADD CONSTRAINT FK_UserProfile_User_status_id FOREIGN KEY (User_status_id) 
REFERENCES UserStatus (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE UserProfile
ADD CONSTRAINT FK_UserProfile_Languages_id FOREIGN KEY (Languages_id) 
REFERENCES Languages (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE UserProfile
ADD CONSTRAINT FK_UserProfile_Sex_id FOREIGN KEY (Sex_id) 
REFERENCES Sex (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE WeekDaysConnection
ADD CONSTRAINT FK_WeekDaysConnection_User_home_id FOREIGN KEY (User_home_id) 
REFERENCES UserHome (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE WeekDaysConnection
ADD CONSTRAINT FK_WeekDaysConnection_Week_days_name_id FOREIGN KEY (Week_days_name_id) 
REFERENCES WeekDaysName (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

 
GO 
ALTER TABLE Messager
ADD CONSTRAINT FK_Messager_First_user_profile_id FOREIGN KEY (First_user_profile_id) 
REFERENCES UserProfile (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE Messager
ADD CONSTRAINT FK_Messager_Second_user_profile_id FOREIGN KEY (Second_user_profile_id) 
REFERENCES UserProfile (Id)
ON DELETE NO ACTION 
ON UPDATE NO ACTION; 
GO

 
GO 
ALTER TABLE Invitation
ADD CONSTRAINT FK_Invitation_First_user_profile_id FOREIGN KEY (First_user_profile_id) 
REFERENCES UserProfile (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO


GO 
ALTER TABLE Invitation
ADD CONSTRAINT FK_Invitation_Second_user_profile_id FOREIGN KEY (Second_user_profile_id) 
REFERENCES UserProfile (Id)
ON DELETE NO ACTION 
ON UPDATE NO ACTION; 
GO


GO 
ALTER TABLE Photo
ADD CONSTRAINT FK_Photo_Photo_type_id FOREIGN KEY (Photo_type_id) 
REFERENCES PhotoType (Id)
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

GO 
ALTER TABLE  UserProfile
ADD CONSTRAINT FK_AspNetUsers_Registration_id FOREIGN KEY (UserRegistration_id) 
REFERENCES [dbo].[AspNetUsers](Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

 
GO 
ALTER TABLE Trip 
ADD CONSTRAINT FK_Trip_User_profile_id FOREIGN KEY (User_profile_id) 
REFERENCES UserProfile (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO 


GO 
ALTER TABLE TripWish 
ADD CONSTRAINT FK_TripWish_Trip_id FOREIGN KEY (Trip_id) 
REFERENCES Trip (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO 


GO 
ALTER TABLE TripItem 
ADD CONSTRAINT FK_TripItem_Trip_id FOREIGN KEY (Trip_id) 
REFERENCES Trip (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO 


GO 
ALTER TABLE ToDoList 
ADD CONSTRAINT FK_ToDoList_Trip_id FOREIGN KEY (Trip_id) 
REFERENCES Trip (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO

GO 
ALTER TABLE PrivacySettings
ADD CONSTRAINT FK_PrivacySettings_User_profile_id FOREIGN KEY (User_profile_id) 
REFERENCES UserProfile (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE; 
GO 